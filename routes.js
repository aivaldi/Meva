module.exports = function (app, apiVersion) {

    /**
     Requires de api
     */

    var apiClient = require('./routes/' + apiVersion + '/manage/client');
    var apiBrand = require('./routes/' + apiVersion + '/manage/brand');
    var apiOrder = require('./routes/' + apiVersion + '/manage/order');
    var apiProduct = require('./routes/' + apiVersion + '/manage/product');
    var apiStock = require('./routes/' + apiVersion + '/manage/stock');
    var apiBn = require('./routes/' + apiVersion + '/manage/billNumbers');

	var apiCalculate = require('./routes/' + apiVersion + '/process/processRoute');

	var information = require('./routes/' + apiVersion + '/info/informationRoute');

	app.use('/api/' + apiVersion, apiClient);

    app.use('/api/' + apiVersion, apiBrand);

    app.use('/api/' + apiVersion, apiOrder);

    app.use('/api/' + apiVersion, apiProduct);

    app.use('/api/' + apiVersion, apiStock);

    app.use('/api/' + apiVersion, apiBn);

	app.use('/api/' + apiVersion, apiCalculate);

	app.use('/api/' + apiVersion, information);


}