var mongoose = require("mongoose");
var Promise = require("promise");
var jwt = require("jwt-simple");
var bcrypt = require("bcrypt");

var ob = require("./orderBill.js");
var stockModel = require("./stock.js");
var clientModel = require("./client.js");

/**
Es la orden de compra 
#los productos los saco de stock es por eso que duplico los puntos y precios al momento.
*/
var OrderSchema = new mongoose.Schema({ 
	client: { type: mongoose.Schema.Types.ObjectId, ref: "ClientSchema" },
	number: Number,
	products:[{
		product: { type: mongoose.Schema.Types.ObjectId, ref: "ProductSchema" },
		points:Number,
		price: Number,
		sellPrice:Number,
		cant:Number
	}],
	total:Number,
	totalPoints:Number,
	totalToPaid:Number,
	totalAdministrative:Number,
	status:String,
	date:Date,
	audit:{}
}, { collection: "OrderSchema" });

/**
 * Antes de guardar genera estas modificaciones
 * Incrementa el contador de las ordenes de compra
 * Modifica el Stock
 */
OrderSchema.pre("save", function(next) {

	ob.findOne({type:"oc"}).exec().then (
		function(ok){
			ok.number+=1;
			ok.save();
		},
		function(error){
			console.log (error);
		}
	);

	this.products.forEach(function(product){

		console.log("*****")
		console.log(product)

		stockModel.findOne({product:product.product.toString()}, function(err, doc){
			console.log(err)
			if (!err)
			{
				console.log(product.product.toString(), doc, product.cant);
				doc.cantProducts-=product.cant;
				doc.save();
			}

		})
	})

	next();
});


/**
 * Despues de guardar genera estas modificaciones
 * Se saca los puntos al cliente
 * Modifica el Stock
 */
OrderSchema.post("save", function() {
	var oc = this;
	clientModel.findOne({"_id":this.client}).exec().then (
		function(c){

			c.calculatedPoints = Number((c.calculatedPoints - oc.totalPoints).toFixed(2))

			if (c.calculatedPoints< 0)
				c.calculatedPoints=0;

			c.save();
		},
		function(error){
			console.log (error);
		}
	);

});

module.exports = mongoose.model("OrderSchema", OrderSchema);
