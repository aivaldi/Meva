var mongoose = require('mongoose');
var Promise = require('promise');
var jwt = require('jwt-simple');
var bcrypt = require('bcrypt');

var StockSchema = new mongoose.Schema({ 
                 product: { type: mongoose.Schema.Types.ObjectId, ref: 'ProductSchema' },
                 price: Number,
                 sellPrice:Number,
                 points:Number,
                 cantProducts:Number,
                 audit:{}
}, { collection: 'StockSchema' });


module.exports = mongoose.model('StockSchema', StockSchema);
