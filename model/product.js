var mongoose = require('mongoose');
var Promise = require('promise');
var jwt = require('jwt-simple');
var bcrypt = require('bcrypt');

var ProductSchema = new mongoose.Schema({ 
                 name: String,
                 description: String,
                 brand: { type: mongoose.Schema.Types.ObjectId, ref: 'ProductBrandSchema' },
                 category : String,
                 code:String,
                 audit:{}
}, { collection: 'ProductSchema' });


module.exports = mongoose.model('ProductSchema', ProductSchema);
