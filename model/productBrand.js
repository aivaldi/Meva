var mongoose = require('mongoose');
var Promise = require('promise');
var jwt = require('jwt-simple');
var bcrypt = require('bcrypt');

var ProductBrandSchema = new mongoose.Schema({ 
                 name: String,
                 description: String,
                 audit:{}
}, { collection: 'ProductBrandSchema' });


module.exports = mongoose.model('ProductBrandSchema', ProductBrandSchema);
