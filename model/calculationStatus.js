var mongoose = require('mongoose');
var jwt = require('jwt-simple');
var Audit = require('../model/audit.js')
var Exception = require('../Exception/MevaException')
const winston = require('winston');

var CalculationStatusSchema = new mongoose.Schema({
                 to: Date,
                 from:Date
}, { collection: 'CalculationStatusSchema' });


/**
 * Dada la ejecucion busca las oc que estan en el periodo
 */
CalculationStatusSchema.methods.getOrders = function() {
	console.log("*************" + this.to)
	console.log("*************" + this.from)
    var dateFilter = {
        $lte: this.to
    }
    if (this.from)
        dateFilter['$gte']=this.from;
    return this.model('OrderSchema').find(
        { 'date':
            dateFilter
        })
};



/**
 * creao el audit
 * actualizo la fecha de ejecucion previa
 * actualizo la fecha de ejecucion actual
 */
CalculationStatusSchema.pre('save',function(next) {
    if (this.audit==null || this.audit==undefined || Object.keys(this.audit).length===0)
        this.audit = Audit.newAudit()
    else
        this.audit = Audit.updateAudit(this.audit,"PROCESS")


    next();
});

module.exports = mongoose.model('CalculationStatusSchema', CalculationStatusSchema);
