var mongoose = require('mongoose');
var Promise = require('promise');
var jwt = require('jwt-simple');
var bcrypt = require('bcrypt');

var UserSchema;
UserSchema = new mongoose.Schema({
	login: String,
	password: String,
	active: Boolean,
	userType: String,
	lastLogin: Date,
	lastToken: String,
	authenticationType: String,
	alias: String


}, {collection: 'User'});

// Saves the user's password hashed (plain text password storage is not good)
UserSchema.pre('save', function (next) {  
  var user = this;
  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        return next(err);
      }
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) {
          return next(err);
        }
        user.password = hash;
        next();
      });
    });
  } else {
    return next();
  }
});



// Create method to compare password input to password saved in database
UserSchema.methods.comparePassword = function(pw, cb) {  
  bcrypt.compare(pw, this.password, function(err, isMatch) {
    if (err) {
      return err;
    }
    return isMatch;
  });
};

// Create method to compare password input to password saved in database
UserSchema.methods.updateToken = function(token) {  
  this.lastToken=token;
  this.lastLogin=new Date()
  var me = this;
  return new Promise(function(fulfill, reject){
    me.save( function(err){
      if (err) reject(err);
      fulfill(me);
    });
  });
};

// Create method to compare password input to password saved in database
UserSchema.methods.renewToken = function() {  
  this.lastLogin=new Date()
  var me = this;
  return new Promise(function(fulfill, reject){
    me.save( function(err){
      if (err) reject(err);
      fulfill(me);
    });
  });
};

UserSchema.statics.authenticate = function(user, password){
  var me=this;
  console.log(password)
  return new Promise(function(fulfill, reject){
    me.findOne({'login':user}, function (err, post) {
      var date = new Date();
      var current_hour = date.getHours();
      if (err) return next(err);        
      if(post != undefined && post!=null)
        {  
          
          if (bcrypt.compareSync(password,post.password))
            {
              console.log(password,post);
              console.log("actualizando token")
              fulfill(post)     
            }
          else
            reject('error');        
        }
      else      
        reject('error');
  });  

  })
  

}

module.exports = mongoose.model('User', UserSchema);
