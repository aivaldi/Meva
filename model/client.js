var mongoose = require("mongoose");
var jwt = require("jwt-simple");

/**
 * Status de los clientes:
 *  0: consumidor
 *  1: Lider
 *  2: Empresario
 *  3: Director
 *  4: Ejecutivo Jr
 *  5: " Sr
 *  6: " Master
 *  7: " VP
 *  8: " Presidente
 * @type {mongoose.Schema}
 */
var ClientSchema = new mongoose.Schema({
	name: String,
	lastName: String,
	dni: String,
	line: String,
	email: String,
	codeNumber: Number,
	street: String,
	number: Number,
	comuna: String,
	phone: String,
	province: String,
	city: String,
	cel: String,
	zip: String,
	parent: [{type: mongoose.Schema.Types.ObjectId, ref: "ClientSchema"}],
	status: {type: Number, default: 0},
	lidershipStatus: {type: Number, default: 0}, //Es el estado de liderazgo
	calculatedPoints: {type: Number, default: 0},
	audit: {
		version: Number,
		lastUpdate: Date,
		created: Date,
		createdBy: String,
		updatedBy: String,
	},
	birthday: Date
}, {collection: "Client"});


/**
 * Dada la ejecucion busca las oc que estan en el periodo
 */
ClientSchema.methods.getChilds = function () {
	return this.model("Client").find(
		{
			"parent": this._id
		});
};
/**
 * Determina el proximo estado
 * la funcion evaluateState dice el estado en el que esta para los puntos dados, esta funcion
 * solo evalua los puntos por lo tanto un cliente esta en estado 0 puede tener puntos de estado 3 (consumidos a Director)
 * pero como los estados tienen que ser corridos, si tiene puntos mayores al actual solo pasa al que sigue
 * una vez que se llega a director el ascenso es distinto.(state 3 es director) TODO
 */
ClientSchema.methods.nextState = function (individualPoints, groupalPoints) {
	console.log("current status " + this.status)
	var r = this.status
	if (this.status < 3)
		if (this.evaluateState(individualPoints, groupalPoints) > this.status)
			r = this.status + 1;

	console.log("new status " + r)
	return r;

};
ClientSchema.methods.nextLidershipState = function (directorList) {
	console.log("get lidership next state " + this.lidershipStatus)
	var r = this.lidershipStatus
	if (this.status < 5)
		if (this.evaluateLidershipState(directorList) > this.lidershipStatus)
			r = this.lidershipStatus + 1;

	console.log("new lidershipStatus " + r)
	return r;

};


/**
 * Determina la categoria en la que esta segun venta individuak y gruapal
 */
ClientSchema.methods.evaluateState = function (individualPoints, groupalPoints) {
	var ret = 0;
	console.log(individualPoints, groupalPoints);

	if (individualPoints >= 10 && groupalPoints >= 200)
		ret = 1;

	if (individualPoints >= 30 && groupalPoints >= 1000)
		ret = 2;

	if (individualPoints >= 50 && groupalPoints >= 5000)
		ret = 3;

	return ret;

};


/**
 * Determina la categoria en la que esta segun venta individuak y gruapal
 */
ClientSchema.methods.evaluateLidershipState = function (directorList) {
	var ret = 0;

	if (directorList.length==0)
		return 0;

	var directorLevel1=0
	var directorLevel2=0
	var directorLevel3=0

	if (directorList.length>=3)
		directorLevel3 = directorList[2].length;
	if (directorList.length>=2)
		directorLevel2 = directorList[1].length;
	if (directorList.length>=1)
		directorLevel1 = directorList[0].length;

	//ejecutivo Jr
	if (directorLevel1>=2)
		ret = 1;

	//ejecutivo Sr
	if (directorLevel1>=4 && directorLevel2>=5)
		ret = 2;


	//ejecutivo Master
	if (directorLevel1>=8 && directorLevel2>=10 && directorLevel3>=10)
		ret = 3;


	//ejecutivo Vp
	if (directorLevel1>=15 && directorLevel2>=25 && directorLevel3>=50)
		ret = 4;


	//ejecutivo Pr
	if (directorLevel1>=25 && directorLevel2>=100 && directorLevel3>=200)
		ret = 5;

	return ret;

};

/**
 * Determina el proximo estado
 */
ClientSchema.methods.calculatePoints = function (groupalPoints) {

	switch (Number(this.status)) {
		case 0:
			return 0;
		case 1:
			return groupalPoints[0] * 0.15; //es el 15% de los consumidores
		case 2:
			return groupalPoints[1] * 0.15 //15% de los lideres
				+ groupalPoints[2] * 0.25; //%25 de los consumidores
		case 3:
			return groupalPoints[2] * 0.15 //15% de los Empresarios
				+ groupalPoints[1] * 0.25; //%25 de los lideres
			+groupalPoints[0] * 0.40; //%40 de los consumidores
	}


	return 0;

};

/**
 * Crea un grafo de los niveles jerarquicos
 * @param client
 * @param clients
 * @param success
 * @param error
 */
function buildGraph(client, clients, success, error) {

	var node = {item: client, childs: []};
	var nodeHash = {};

	clients.forEach(function (c) {
		var parent = c.parent[c.parent.length - 1];

		if (!nodeHash[c._id])
			nodeHash[c._id] = {item: c, childs: []};
		else
			nodeHash[c._id].item = c;
		if (parent.toString() == client._id.toString()) {
			node.childs.push(nodeHash[c._id]);
		}
		else {
			if (!nodeHash[parent])
				nodeHash[parent] = {childs: []};

			nodeHash[parent].childs.push(c);
		}
	});

	console.log(node);

	success(node);

}


/**
 * Crea un grafo de los niveles jerarquicos
 *
 * Genero los puntos y hago los calculos de los directores
 *
 * @param client
 * @param clients
 * @param success
 * @param error
 */
function buildGraphStatus(client, clients, orders, success, error) {
	if (!orders)
		orders = {}
	var ret = [];
	var node = {item: client, childs: [], points: orders[client._id.toString()], directorChild: []};
	ret.push(node);
	var nodeHash = {};

	//me aseguro que un hijo no este antes que un padre
	clients = clients.sort(function (a, b) {
		return a.parent.length - b.parent.length;
	});

	clients.forEach(function (c) {
		var parent = c.parent[c.parent.length - 1];

		//agrego el nodo al hash de nodos
		if (!nodeHash[c._id])
			nodeHash[c._id] = {item: c, childs: [], points: orders[c._id] ? orders[c._id] : 0};
		else
			nodeHash[c._id].item = c;


		if (parent.toString() == client._id.toString()) {
			if (client.name == "MEVA") {
				node.childs.push(nodeHash[c._id]);
			}
			else
			//si esta al nivel del padre sube
			if (client.status <= c.status)
				ret.push(nodeHash[c._id]);
			else {
				node.childs.push(nodeHash[c._id]);
			}
		} else {
			var i = c.parent.length - 1;
			var originI = i;
			//busca al nivel de que padre esta, si esta al mismo nivel que el padre busca otros padres
			while (i > 0 && nodeHash[c.parent[i]].item.status <= c.status) {
				i--;
			}
			console.log('evualating state for', c.name, 'status', node.item.status, 'move level', i)
			//si esta a nivel del primero sube
			if (i == 0) {
				if (client.name == "MEVA")
					node.childs.push(nodeHash[c._id]);
				else
					ret.push(nodeHash[c._id]);
				//addDirectorList(nodeHash, c, originI);
			}
			else {
				nodeHash[c.parent[i]].childs.push(nodeHash[c._id]);
				//addDirectorList(nodeHash, c, originI)
			}

		}

	});
	ret = ret.map(function (node) {
		buildPointsForClientGraph(node, null);
		return node;
	});

	//en base al graph original calculo los puntos y el estado
	var nodeReal = {item: client, childs: [], points: orders[client._id.toString()]};
	var nodeHashReal = {};

	clients.forEach(function (cl) {
		var nh = nodeHash[cl._id.toString()];
		var c = nh.item;
		var parent = c.parent[c.parent.length - 1];

		if (!nodeHashReal[c._id])
			nodeHashReal[c._id] = {
				item: c, childs: [], points: orders[c._id] ? orders[c._id] : 0,
				calculatedPoints: nh.calculatedPoints, groupalPoints: nh.groupalPoints
			};
		else
			nodeHashReal[c._id].item = c;

		var i = c.parent.length - 1;
		var originI = i;

		if (parent.toString() == client._id.toString()) {
			nodeReal.childs.push(nodeHashReal[c._id]);
			addDirectorList(nodeHashReal, c, originI);
		}
		else {
			if (!nodeHashReal[parent])
				nodeHashReal[parent] = {childs: []};

			nodeHashReal[parent].childs.push(nodeHashReal[c._id]);
			addDirectorList(nodeHashReal, c, originI);
		}
	});

	ret = buildPointsForClientGraph(nodeReal, false);

	Object.keys(nodeHashReal).forEach(function (node_key) {
		var node = nodeHashReal[node_key]
		node.newStatus = node.item.nextState(node.points, node.groupalPoints)
	})

	//hay que calcular los puntos segun los directores y lo que sobre del % total.

	success(
		[nodeReal]
	);

}

/**
 * Tengo que agregar los nodos que corresponden en el nivel de director que tiene
 * tiene que ser [[]] el primer array es el nivel el segundo el listado de los nodos en ese nivel
 * @param nodeHash
 * @param c cliente donde esta
 * @param i posicion del padre
 */
function addDirectorList(nodeHash, c, i) {
	//si es director entonces lo agrego al padre como director de segundo nivel
	if (i == 0)
		return;
	if (nodeHash[c._id].item.status < 3)
		return;

	// i me dice la cantidad de padres que tengo, hay que agregar al director en la posicion que corresponde

	for (var pos = 0; pos < i; pos++) {
		if (!nodeHash[c.parent[i-pos]].directorChild)
			nodeHash[c.parent[i-pos]].directorChild = [[]];
		//si estoy en una posicion que el padre todavia no tiene, la tengo que crear
		//ejemplo Si el director es nivel 3 y no tiene nivel 2 y 1, no tiene array hay que crear los espacion en blanco
		for (; pos+1 > nodeHash[c.parent[i-pos]].directorChild.length;)
			nodeHash[c.parent[i-pos]].directorChild.push([]);

		var parentDC = nodeHash[c.parent[i-pos]].directorChild;
		console.log(parentDC,i,parentDC[0+pos])
		parentDC[0+pos].push(nodeHash[c._id])

		/*
		 var childDC = nodeHash[c._id].directorChild;

		 //si tiene drectores el hijo entonces hay que copiar los niveles
		 if (childDC) {

		 var minArray = (parentDC.length < childDC.length) ? parentDC : childDC;
		 var maxArray = (parentDC.length >= childDC.length) ? parentDC : childDC;
		 //duplico el array mas grande
		 var resDC = JSON.parse(JSON.stringify(maxArray));

		 for (var pos = 1; pos <= minArray.length; pos++) {
		 //el array interno
		 var arrayPosition = resDC[resDC.length - pos];
		 arrayPosition.push.apply(arrayPosition, minArray[minArray.length - pos])
		 }
		 nodeHash[c.parent[i]].directorChild = resDC;


		 }*/
		//agrego el actual


	}
}

//llamada recusriva para poder obtener los puntos
function buildPointsForClientGraph(node, notOverrideCalculatedPoints) {

	//si no tiene hijos devuelve sus puntos solamente
	if (node.childs.length == 0) {
		node.groupalPoints = 0;
		return {own: node.points, childGroupalPoints: 0, directorPoints: []};
	}
	else {
		if (notOverrideCalculatedPoints != false)
			node.calculatedPoints = 0;
		var groupalAcum = (node.groupalPoints ? node.groupalPoints : 0);
		//recorro todos los hijos
		node.childs.forEach(function (nodeChild) {
			var points = buildPointsForClientGraph(nodeChild, notOverrideCalculatedPoints);
			if (notOverrideCalculatedPoints != false) {
				node.calculatedPoints += (  points.own * ( getChildFactor(node.item.status, nodeChild.item.status) ) +
					points.childGroupalPoints * 0.15) * 0.25;

				groupalAcum += points.own + points.childGroupalPoints;
			}
		});
		if (notOverrideCalculatedPoints != false)
			node.groupalPoints = groupalAcum;

		if (notOverrideCalculatedPoints == true) {
			//hay que agregarle los puntos indirectos por director si tiene
			console.log('**** puntos directorPonits', node.item.name, node.directorChild)
			var directorDeep = 1;//es la indireccion del director
			if (node.directorChild)
				for (var pos = 0; pos < node.directorChild.length ; pos++)
					for (var pos2 = 0; pos2 < node.directorChild[pos].length ; pos2++) {
						if (node.directorChild[pos2].calculatedPoints )
							switch (pos2+1) {
							case 1:
								node.calculatedPoints += node.directorChild[pos2].calculatedPoints * 0.1 * 0.25;
								break;
							case 2:
								node.calculatedPoints += node.directorChild[pos2].calculatedPoints * 0.02 * 0.25;
								break;
							case 3:
								node.calculatedPoints += node.directorChild[pos2].calculatedPoints * 0.01 * 0.25;
								break;
							case 4:
								node.calculatedPoints += node.directorChild[pos2].calculatedPoints * 0.01 * 0.25;
								break;
						}

				}
			node.item.lidershipStatus= node.item.nextLidershipState(node.directorChild);
		}
		var ret = {
			own: node.points,
			childGroupalPoints: groupalAcum
		};

		console.log('calcuating poitns for', node.item.name, ret, node.calculatedPoints)
		return ret;
	}

}

/**
 * Dado dos nodos devuelve el factor de % que gana a partir
 * @param nodeStauts
 * @param nodeChildStauts
 */
function getChildFactor(nodeStatus, nodeChildStauts) {
	if (nodeStatus == 3) {
		if (nodeChildStauts == 2)
			return 0.15;

		if (nodeChildStauts == 1)
			return 0.25;

		if (nodeChildStauts == 0)
			return 0.40;

	}
	if (nodeStatus == 2) {
		if (nodeChildStauts == 1)
			return 0.15;

		if (nodeChildStauts == 0)
			return 0.25;
	}
	if (nodeStatus == 1) {
		return 0.15;
	}
	return 0;
}

/**
 * Arma la estructura dado el id
 */
ClientSchema.statics.getGraph = function (id) {
	var me = this;
	return new Promise(function (success, error) {
		me.findOne({"_id": id}, function (err, client) {
			me.find({"parent.1": id}, function (err, clients) {
				buildGraph(client, clients, success, error);
			});

		});
	});

};

/**
 * Arma la estructura dado el id
 */
ClientSchema.statics.getGraphStatus = function (id) {
	var me = this;
	return new Promise(function (success, error) {
		me.findOne({"_id": id}, function (err, client) {
			var parentPosition = client.parent.length;
			var obId = {}
			obId ["parent." + parentPosition.toString()] = id;
			console.log(obId, client.parent)
			me.find(obId, function (err, clients) {
				buildGraphStatus(client, clients, null, success, error);
			});

		});
	});

};


/**
 * Arma la estructura dado el id
 * toma las ordenes y calcula el % de descuento que tienen para ese monto
 */
ClientSchema.statics.getGraphStatusWithOrder = function (id, orders) {
	var me = this;
	return new Promise(function (success, error) {
		me.findOne({"name": 'MEVA'}, function (err, client) {
			var parentPosition = client.parent.length;
			var obId = {}
			obId ["parent." + parentPosition.toString()] = client._id.toString();
			console.log(obId, client.parent)
			me.find(obId, function (err, clients) {
				buildGraphStatus(client, clients, orders, success, error);
			});

		});
	});

};

/**
 * itero el arbol y lo guardo
 */
ClientSchema.statics.saveGraphStatusWithOrder = function (nodes) {

	var me = this;
	nodes.forEach(function (node) {
		me.findOne({"_id": node.item._id.toString()}, function (err, client) {
			client.calculatedPoints = node.calculatedPoints;
			client.status = node.newStatus;
			console.log("saving clients...", client.name, client.status, client.calculatedPoints)
			client.save();
		})

		me.saveGraphStatusWithOrder(node.childs);
	});


};

module.exports = mongoose.model("ClientSchema", ClientSchema);
