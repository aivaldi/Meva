module.exports = {

	newAudit : function(user){
		return {
			version:1,
			lastUpdate:Date.now(),
			created:Date.now(),
			createdBy : user
		}
	},

	updateAudit : function(audit, user){
		audit.version=audit.version+1
		audit.lastUpdate=Date.now()
		audit.updatedBy = user
		
		return audit
	}

};