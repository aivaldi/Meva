'use strict';


//  Modified from https://github.com/elliotf/mocha-mongoose

var mongoose = require('mongoose');


// Use native Node promises
mongoose.Promise = global.Promise;

// ensure the NODE_ENV is set to 'test'
// this is helpful when you would like to change behavior when testing
process.env.NODE_ENV = 'test';


beforeEach(function (done) {



    if (mongoose.connection.readyState === 0) {
        mongoose.connect('mongodb://localhost/meva') //Heroku
        //mongoose.connect(config.database)
            .then(() =>  console.log('connection succesful'))
        .catch((err) => console.error(err));
    }
});


afterEach(function (done) {
    mongoose.disconnect();
    return done();
});