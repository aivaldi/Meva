app.filter('ownername', function() {
  return function(input) {
    if (input==null)
      return ""
    
    if (input.alias!=null)
      return input.alias

    return input.login

  };
})