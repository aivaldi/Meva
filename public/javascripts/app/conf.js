app.constant('AUTH_EVENTS', {
  notAuthenticated: 'auth-not-authenticated'
  });
app.service('AuthService', function($q, $http) {
  var LOCAL_TOKEN_KEY = 'runnerAppTokenKey';
  var isAuthenticated = false;
  var authToken;
 
  function loadUserCredentials() {
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    if (token) {
      useCredentials(token);
    }
  }
 
  function storeUserCredentials(token) {
    window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
    useCredentials(token);
  }
 
  var storeCredentials=function(token){
    storeUserCredentials(token);
  }
  function useCredentials(token) {
    isAuthenticated = true;
    authToken = token;
 
    // Set the token as header for your requests!
    $http.defaults.headers.common.Authorization = authToken;
  }
 
  function destroyUserCredentials() {
    authToken = undefined;
    isAuthenticated = false;
    $http.defaults.headers.common.Authorization = undefined;
    window.localStorage.removeItem(LOCAL_TOKEN_KEY);
  }
  
  var login = function(user) {
    return $q(function(resolve, reject) {
      $http.post('/token', user).then(function(result) {
        if (result.data.success) {
          storeUserCredentials(result.data.token);
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };
 
  var logout = function() {
    destroyUserCredentials();
  };
 
  loadUserCredentials();
 
  return {
    login: login,    
    logout: logout,
    storeUserCredentials:storeCredentials,
    isAuthenticated: function() {return isAuthenticated;},
  };
});
app.factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
  return {
    responseError: function (response) {
      $rootScope.$broadcast({
        401: AUTH_EVENTS.notAuthenticated,
      }[response.status], response);
      return $q.reject(response);
    }
  };
}) 
.config(function ($httpProvider) {
  $httpProvider.interceptors.push('AuthInterceptor');
});
/**
configuracion de temas
*/
app.config(function ($mdThemingProvider) {
    
$mdThemingProvider.definePalette('mcgpalette0', {
  '50': 'f3e0e0',
  '100': 'e0b3b3',
  '200': 'cc8080',
  '300': 'b84d4d',
  '400': 'a82626',
  '500': '990000',
  '600': '860000',
  '700': '860000',
  '800': '7c0000',
  '900': '6b0000',
  'A100': 'ff9a9a',
  'A200': 'ff6767',
  'A400': 'ff3434',
  'A700': 'ff1a1a',
  'contrastDefaultColor': 'light',
  'contrastDarkColors': [
    '50',
    '100',
    '200',
    'A100',
    'A200'
  ],
  'contrastLightColors': [
    '300',
    '400',
    '500',
    '600',
    '700',
    '800',
    '900',
    'A400',
    'A700'
  ]
});
$mdThemingProvider.definePalette('mcgpalette1', {
  '50': 'e8e7e7',
  '100': 'c6c4c2',
  '200': 'a09d9a',
  '300': '797671',
  '400': '5d5852',
  '500': '403b34',
  '600': '3a352f',
  '700': '322d27',
  '800': '2a2621',
  '900': '1c1915',
  'A100': 'ffb163',
  'A200': 'ff9730',
  'A400': 'fc7e00',
  'A700': 'e27100',
  'contrastDefaultColor': 'light',
  'contrastDarkColors': [
    '50',
    '100',
    '200',
    '300',
    '400',
    '500',
    '600',
    'A100',
    'A200',
    'A400',
    'A700'
  ],
  'contrastLightColors': [
    '700',
    '800',
    '900'
  ]
});


$mdThemingProvider.theme('default')
    .primaryPalette('mcgpalette0')
    .accentPalette('mcgpalette1');

});
