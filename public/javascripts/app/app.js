var apiURL='/api/1'
app = angular.module('demoApp', 
	['ngResource',
	'ui.router',
	'ngMaterial',
  'ngMessages',
  'ngMap','htmlToPdfSave'
	]);

app.factory('httpRequestInterceptor', function () {
  return {
    request: function (config) {
      if (token!=null)
        config.headers['Authorization'] = 'JWT '+token;
      
      return config;
    }
  };
});

app.factory('httpRequestInterceptorError', function ($q) {
  return {
    response: function (response) {
      if (response.headers('content-type')!=null && response.headers('content-type').indexOf("application/json")>=0)
        {
            if (response.data==null)
              return response;

            if (response.data instanceof Object && response.data.Result=='Failure')
              return $q.reject(response);
            return response;    
        }
      else
        return response;
    }
  };
});

app.config(function ($httpProvider) {
  $httpProvider.interceptors.push('httpRequestInterceptor');
  $httpProvider.interceptors.push('httpRequestInterceptorError');
});