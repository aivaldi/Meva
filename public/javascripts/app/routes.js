app.config(function($stateProvider,$urlRouterProvider) {	  
	  $stateProvider.state('login',{
      url: '/login',
      templateUrl: 'partials/loginform.html'
        })

	  $stateProvider.state('home',{
      url: '/home',
      templateUrl: 'partials/home/index.html'
        })


    $stateProvider.state('brand',{
      url: '/Marca',
      templateUrl: 'partials/manage/brand/index.html'
        })


    $stateProvider.state('product',{
      url: '/Producto',
      templateUrl: 'partials/manage/product/index.html'
        })

	$stateProvider.state('stock',{
		url: '/Stock',
		templateUrl: 'partials/manage/stock/index.html'
	})

	$stateProvider.state('stockUpdate',{
		url: '/StockUpdate',
		templateUrl: 'partials/manage/stockUpdate/index.html'
	})


    $stateProvider.state('client',{
      url: '/Cliente',
      templateUrl: 'partials/manage/client/index.html'
        })

    $stateProvider.state('newOrder',{
      url: '/Orden/Nueva',
      templateUrl: 'partials/order/new/index.html'
        })


    $stateProvider.state('queryOrder',{
        url: '/Orden/Consulta',
        templateUrl: 'partials/order/query/index.html'
    })
    $stateProvider.state('graphPoints',{
        url: '/Calculation/Graph/',
        templateUrl: 'partials/calculation/graph/index.html'
    })


	$stateProvider.state('graphPointsEvaluate',{
		url: '/Calculation/GraphPointsAt',
		templateUrl: 'partials/calculation/graphPointsAt/index.html'
	})


	$stateProvider.state('productsSell',{
		url: '/Calculation/product/sell',
		templateUrl: 'partials/information/product/sell.html'
	})


	$stateProvider.state('pointList',{
		url: '/Calculation/point/list',
		templateUrl: 'partials/information/point/list.html'
	})


	$stateProvider.state('ordersChild',{
		url: '/Calculation/client/orders/child',
		templateUrl: 'partials/information/client/list.html'
	})


	$stateProvider.state('clientData',{
		url: '/Calculation/client/data',
		templateUrl: 'partials/information/client/data.html'
	})






      
      
      //$urlRouterProvider.otherwise('login');
});
