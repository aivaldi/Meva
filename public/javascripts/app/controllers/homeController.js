app
  .controller('HomeController', ['$state', HomeController]);

  function HomeController(_$state){

  	this.$state=_$state;

  
  }

HomeController.prototype.home = function (){
	this.$state.go('home');	
}

HomeController.prototype.brandShow=function(){
	console.log('brand');
	this.$state.go('brand');	
}


HomeController.prototype.productShow=function(){
	console.log('product');
	this.$state.go('product');	
}

HomeController.prototype.stockShow=function(){
	console.log('stock');
	this.$state.go('stock');	
}

HomeController.prototype.stockUpdateShow=function(){
	console.log('stockUpdate');
	this.$state.go('stockUpdate');
}

HomeController.prototype.clientShow=function(){
	console.log('client');
	this.$state.go('client');	
}

HomeController.prototype.newOrderShow=function(){
	console.log('newOrder');
	this.$state.go('newOrder');	
}
HomeController.prototype.queryOrderShow=function(){
    console.log('queryOrder');
    this.$state.go('queryOrder');
}
HomeController.prototype.graphPoints=function(){
	console.log('graphPoints');
	this.$state.go('graphPoints');
}


HomeController.prototype.graphPointsEvaluate=function(){
	console.log('graphPointsEvaluate');
	this.$state.go('graphPointsEvaluate');
}


HomeController.prototype.productsSell=function(){
	console.log('productsSell');
	this.$state.go('productsSell');
}


HomeController.prototype.pointList=function(){
	console.log('pointList');
	this.$state.go('pointList');
}


HomeController.prototype.clientOrderChild=function(){
	this.$state.go('ordersChild');
}

HomeController.prototype.clientData=function(){
	this.$state.go('clientData');
}