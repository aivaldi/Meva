app.controller('ProductController',['$scope','$mdDialog','ProductFactory', 'BrandFactory','ErrorService',ProductController] )

function ProductController(_$scope,_$mdDialog, _models ,_brands, _errorService){
	BaseModelController.call(this,_$scope,_$mdDialog, _models, _errorService )

	var me = this;


	_brands.query().$promise.then(
		function (ok){
			console.log(ok)
			me.$scope.brandList = ok._Array
		},
		function (error){
			me.errorService(error);
			console.log("error");
			me.hideLoading()
		}

	)


}

ProductController.prototype = Object.create(BaseModelController.prototype)


ProductController.prototype.getQuery = function(searchTextModel){
	if (searchTextModel && searchTextModel.length>0)
		return {
			name:searchTextModel,
			code:searchTextModel
		};
	else {}
}


/*
ProductController.prototype.loadData = function(){
	var me=this;
	this.$scope.models=[]
	this.$scope.modelFields=[]
	this.showLoading()
	this.modelsResource.query().$promise.then(
		function (ok){
			console.log(ok)
			me.$scope.models = ok._Array
			me.hideLoading()
		},
		function (error){
			me.errorService(error);
			console.log("error");
			me.hideLoading()
		}
	)
}*/





