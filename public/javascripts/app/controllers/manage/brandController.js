app.controller('BrandController',['$scope','$mdDialog', 'BrandFactory','ErrorService',BrandController] )

function BrandController(_$scope,_$mdDialog, _models , _errorService){
	BaseModelController.call(this,_$scope,_$mdDialog, _models, _errorService )
	this.parentField = 'ID_MAPEO'

}



BrandController.prototype = Object.create(BaseModelController.prototype)


BrandController.prototype.getQuery = function(searchTextModel){
	if (searchTextModel && searchTextModel.length>0)
		return {
			name:searchTextModel
		};
	else {}
}
