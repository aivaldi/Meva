app.controller('StockUpdateController',['$scope','$mdDialog','StockFactory', 'BrandFactory','ErrorService',StockUpdateController] )

function StockUpdateController(_$scope,_$mdDialog, _models ,_brand, _errorService){

	var me = this;

	this.$scope = _$scope;
	this.models = _models;
	this.$mdDialog = _$mdDialog;
	this.errorService =_errorService;

	this.$scope.increase=0;
	_brand.query().$promise.then(
		function (ok){

			me.$scope.brandList = ok._Array
		},
		function (error){
			me.errorService(error);
			console.log("error");
			me.hideLoading()
		}

	)



	this.brandChange = function(brandId){
		_models.queryBrand({brand:brandId}).$promise.then(
			function (ok){

				me.$scope.models = ok.map(function(val){ val.selected=true;return val})

			},
			function (error){
				me.errorService(error);
				console.log("error");

			}

		)
	}

	this.updatePrice=function() {
		var idToUpdate = me.$scope.models.filter(function(val){
			return val.selected
		}).map(function(val){
			return val._id.toString()
		})

		toUpdate ={
			increase:this.$scope.increase,
			stocks:idToUpdate
		}

		_models.updateValue(toUpdate).$promise.then(
			function (ok){

				me.brandChange(me.$scope.brand);

			},
			function (error){
				me.errorService(error);
				console.log("error");

			})

	}


}