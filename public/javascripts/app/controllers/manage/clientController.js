app.controller('ClientController',['$scope','NgMap','$mdDialog','ClientFactory','ErrorService',ClientController] )

function ClientController(_$scope,ngMap,_$mdDialog, _models , _errorService){
	BaseModelController.call(this,_$scope,_$mdDialog, _models, _errorService )

	var me = this;


	ngMap.getMap().then(function(map) {
    var latlng = new google.maps.LatLng(40, -110);
       map.setCenter(latlng);
       map.setZoom(4);
       window.map = map;
  });




}


ClientController.prototype = Object.create(BaseModelController.prototype)



ClientController.prototype.getQuery = function(searchTextModel){
	if (searchTextModel && searchTextModel.length>0)
	return {
		name:searchTextModel,
		lastName:searchTextModel,
		dni:searchTextModel
	};
	else {}
}

/**
 * Agrego al array de parents del padre el ide del padre.
 * @param elem
 * @returns {*}
 */
ClientController.prototype.onPreSave = function(elem){
	console.log(elem);
	elem.parent = elem.parentBase.parent.slice(0)
	elem.parent.push(elem.parentBase._id)
	return elem;
}

ClientController.prototype.clientSearch =function (query){
	console.log(query);
	var me = this;
	var queryParams ={
		name:query,
		lastName:query,
		dni:query
	}


	return new Promise( function(success, failure){
		me.modelsResource.query(queryParams).$promise.then(function(ok){
			success(ok._Array)
		}, failure)
	} )

};


ClientController.prototype.onEdit = function(element, secondElement){
	var me = this;
	me.$scope.selected.birthday = new Date(me.$scope.selected.birthday);

	if (me.$scope.selected.parent.length>0)
	this.modelsResource.get({id:me.$scope.selected.parent[me.$scope.selected.parent.length-1]}).$promise.then(
		function (ok){
			console.log(ok)
			me.$scope.selected.parentBase= ok
		},
		function (error){
			me.errorService(error);
			console.log("error");
			me.hideLoading()
		}
	)
}

ClientController.prototype.getLineNumber = function (line){
	var me = this;
	if (line==undefined || line==null)
		return;
	this.modelsResource.getLineNumber({line:line}).$promise.then(
		function (ok){
			console.log(ok)
			me.$scope.selected.codeNumber = ok.value
		},
		function (error){
			me.errorService(error);
			console.log("error");
			me.hideLoading()
		}
	)
}

ClientController.prototype.updateLine = function (line){
	var me = this;

	if (line==undefined || line==null)
		return;

	me.$scope.selected.line = line

	this.modelsResource.getLineNumber({line:line}).$promise.then(
		function (ok){
			console.log(ok)
			me.$scope.selected.codeNumber = ok.value
		},
		function (error){
			me.errorService(error);
			console.log("error");
			me.hideLoading()
		}
	)
}

