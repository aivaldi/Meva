app.controller('StockController',['$scope','$mdDialog','StockFactory', 'ProductFactory','ErrorService',StockController] )

function StockController(_$scope,_$mdDialog, _models ,_product, _errorService){
	BaseModelController.call(this,_$scope,_$mdDialog, _models, _errorService )

	var me = this;


	_product.query().$promise.then(
		function (ok){
			console.log(ok)
			me.$scope.productList = ok._Array
		},
		function (error){
			me.errorService(error);
			console.log("error");
			me.hideLoading()
		}

	)


}

StockController.prototype = Object.create(BaseModelController.prototype)


StockController.prototype.getQuery = function(searchTextModel){
	if (searchTextModel && searchTextModel.length>0)
		return {
			name:searchTextModel
		};
	else {}
}



