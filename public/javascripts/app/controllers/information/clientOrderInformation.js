app.controller("ClientOrderInformationController", ["$scope","ClientFactory", "$mdDialog", ClientOrderInformationController]);

function ClientOrderInformationController(_$scope,_clientFactory, _$mdDialog) {

	this.$scope = _$scope;
	this.clientFactory = _clientFactory;
	this.$mdDialog = _$mdDialog;
	this.pdfView=false;

}


	ClientOrderInformationController.prototype.printPDF = function() {

	}

	ClientOrderInformationController.prototype.getChilds = function(id, date){
	var me=this;
	if (!id || !date)
		return;
	me.clientFactory.getChildsOrders({id:id, date:date.toISOString().slice(0,10).replace(/-/g,"")}).$promise.then(
		function(res){
			me.$scope.childs = res;
		}
		,
		function(err){
			console.log(err)
		}
	)

}


ClientOrderInformationController.prototype.clientSearch =function (query){
	console.log(query);
	var me = this;
	return new Promise(
		function (success, failure){
			me.clientFactory.query({name:query, lastName:query}).$promise.then(
				function(r){success(r._Array)},failure
			)
		}
	)
}
