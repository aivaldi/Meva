app.controller("ClientDataController", ["$scope","ClientFactory", "$mdDialog", ClientDataController]);

function ClientDataController(_$scope,_clientFactory, _$mdDialog) {

	this.$scope = _$scope;
	this.clientFactory = _clientFactory;
	this.$mdDialog = _$mdDialog;


	this.$scope.templates = [
		{
			name:'Fecha de cumpleaños',
			url:'/partials/information/client/data-birthdaty.html',
			id:0
		},
		{
			name:'Ingreso de clientes',
			url:'/partials/information/client/incoming.html',
			id:1
		}
	]
	this.$scope.months=[
		{value:1 ,name:'Enero'},
		{value:2 ,name:'Febrero'},
		{value:3 ,name:'Marzo'},
		{value:4 ,name:'Abril'},
		{value:5 ,name:'Mayo'},
		{value:6 ,name:'Junio'},
		{value:7 ,name:'Julio'},
		{value:8 ,name:'Agosto'},
		{value:9 ,name:'Septiembre'},
		{value:10 ,name:'Octubre'},
		{value:11 ,name:'Noviembre'},
		{value:12 ,name:'Diciembre'}
	]

}

ClientDataController.prototype.templateChange = function(id){
	console.log(id);
	switch (id){
		case 0:
			break;

		case 1:
			break;

	}
}

ClientDataController.prototype.getClientBirthday = function(month){
	var me=this;
	if (!month)
		return;
	me.clientFactory.getByBirthday({date:month}).$promise.then(
		function(res){
			me.$scope.clients = res;
		}
		,
		function(err){
			console.log(err)
		}
	)

}

function treatAsUTC(date) {
	var result = new Date(date);
	result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
	return result;
}

function daysBetween(startDate, endDate) {
	var millisecondsPerDay = 24 * 60 * 60 * 1000;
	return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
}


ClientDataController.prototype.getClientLast  =function(period, dateFrom, dateTo){
	var me=this;
	if (!period || !dateFrom|| !dateTo)
		return;
	var strDateFrom = new Date(dateFrom.getFullYear(),dateFrom.getMonth(),dateFrom.getDate()).toISOString().slice(0,10).replace(/-/g,"");
	var strDateTo = new Date(dateTo.getFullYear(),dateTo.getMonth(),dateTo.getDate()).toISOString().slice(0,10).replace(/-/g,"");

	me.clientFactory.getClientUntil({period:period, from:strDateFrom, to:strDateTo}).$promise.then(
		function(res){
			me.$scope.clients = res;
			var arrayCol = ['x'];
			var dataCant = ['Fecha'];

			var format = ''
			var formatLabel=''
			switch (period) {
				case 'año':
					format = '%Y'
					formatLabel = '%Y'
					break;
				case 'mes':
					format = '%Y%m'
					formatLabel = '%m-%Y'
					break;
				case 'dia':
					format = '%Y%m%d'
					formatLabel = '%d-%m-%Y'
					break;
			}

			arrayCol.push.apply( arrayCol, res.map(function(val){ return val.date }))
			dataCant.push.apply( dataCant, res.map(function(val){ return val.cant }))
			console.log(arrayCol);
			me.chart = c3.generate({
				bindto: '#chart',
				data: {
					x: 'x',
					xFormat: format,//'%Y%m%d', // 'xFormat' can be used as custom format of 'x'
					columns: [
						arrayCol,
						dataCant
					]
				},
				axis: {
					x: {
						type: 'timeseries',
						tick: {
							rotate: 60,

							format: formatLabel
						}
					}
				}
			});

		}
		,
		function(err){
			console.log(err)
		}
	)

}

ClientDataController.prototype.clientSearch =function (query){
	console.log(query);
	return this.clientFactory.query({name:query, lastName:query}).$promise
}
