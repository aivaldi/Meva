app.controller("ProductSellInfomrationController", ["$scope","ProductSellFactory","BrandFactory", "$mdDialog", ProductSellInfomrationController]);

function ProductSellInfomrationController(_$scope,_productSell,_brandFactory, _$mdDialog) {

	this.$scope = _$scope;
	this.productSell = _productSell;
	this.brandFactory = _brandFactory;
	this.$mdDialog = _$mdDialog;


}

ProductSellInfomrationController.prototype.top = function(top) {
	this.updateTop(top);
}

ProductSellInfomrationController.prototype.calculation = function(from, to){
	var me=this;

	if (!from || !to)
		return;



	var query = {}
	var dateTo = new Date(to);
	dateTo.setDate(dateTo.getDate()+1)

	var strDateTo = dateTo.toISOString().slice(0,10).replace(/-/g,"");
	var strDateFrom = from.toISOString().slice(0,10).replace(/-/g,"");
	query['to'] = strDateTo;
	query['from'] = strDateFrom

	if (this.brand && this.brand._id){
		query['id'] = this.brand._id;
		var future = this.productSell.queryBrand(query);
	}
	else
        var future = this.productSell.get(query)

	future.$promise.then(
		function (res) {
			console.log(res);
			me.productsResult = res;
			me.updateTop(0)

		},
		function (cancel) {
			console.log(cancel);
		});

}

ProductSellInfomrationController.prototype.searchBrand = function(query){
	console.log(query);
	var me = this;
	return new Promise(function (  success, failure  ){
		me.brandFactory.query({name:query, lastName:query}).$promise.then(
			function(r){
				success(r._Array);
			},failure
		);
	});
};

ProductSellInfomrationController.prototype.brandChanged = function(){
	this.product=null;
	this.calculation(this.$scope.dateFrom, this.$scope.dateTo);

};

ProductSellInfomrationController.prototype.updateTop = function(top){
	if (!this.productsResult)
		return;

	var res = this.productsResult;
	var me = this;
	var keys = Object.keys(res.toJSON())
	if (top>0)
		keys = Object.keys(res.toJSON()).sort( function(a,b){  return res[a].price<res[b].price } ).slice(0,top)

	var columns=[["venta $"],["cantidad vendidos"]]
	var axis=[]
	me.$scope.list = []
	keys.forEach(function(cKey){
		columns[0].push(res[cKey].price)
		columns[1].push(res[cKey].cant)
		axis.push(cKey + "("+res[cKey].brandName+")")
		me.$scope.list.push({name:cKey, price:res[cKey].price,cant:res[cKey].cant, brandName:res[cKey].brandName })
	})

	var chart = c3.generate({
		bindto: '#chart',
		data: {
			columns: columns,
			types: {
				'cantidad vendidos': 'line',
				'venta $': 'bar'
			},
			axes: {
				'cantidad vendidos': 'y2'
			}
		},
		axis: {
			x: {
				type: 'category',
				categories: axis
			},
			y: {
				label: {
					text: '$',
					position: 'outer-middle'
				},
				tick: {
					format: d3.format("$,") // ADD
				}
			},
			y2: {
				show: true,
				label: {
					text: 'Cantidad',
					position: 'outer-middle'
				}
			}
		}

	});

}

