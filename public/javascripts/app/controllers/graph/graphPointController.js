app.controller("GraphPointController", ["$scope", "CalculationFactory","ClientFactory", "$mdDialog", GraphPointController]);

function GraphPointController(_$scope, _calculation,_clientModel, _$mdDialog) {

	this.$scope = _$scope;
	this.calculationModel = _calculation;
	this.clientModel=_clientModel;
	this.$mdDialog = _$mdDialog;
	this.options = {
		name: "cola"//,
		//maxSimulationTime: 4000,
		//flow:{ axis: 'y', minSeparation: 50 },
		//fit: true
	}

	this.backgroudColorByStatus = function(status){
		switch (status){
			case 0:
				return "#efeca5";
			case 1:
				return "#188a11";
			case 2:
				return "#1d1adc";
			case 3:
				return "#AC5515";

		}
	}

	this.addNode=function(value, parent,_node)
	{
		var me =this;
		var node = [];
		if (_node)
			node=_node;
		var ret = {};
		ret["id"] = value.item._id.toString();
		ret["bg"] = this.backgroudColorByStatus(value.item.status);
		ret["label"] = "Nombre:"+value.item.name +"\n"+"Apellido:"+value.item.lastName+"\n";
		if (value.item.name!="MEVA")
			ret["label"]+="Puntos a favor:"+value.item.calculatedPoints;
		ret["client"] = value.item;
		ret["status"] = value.item.status;
		if (value.item.name!="MEVA")
			ret["points"] = value.points;

		node.push({data: ret});

		if (parent){
			node.push({
				data: {
					id: parent.item._id + value.item._id,
					source: parent.item._id.toString(),
					target: value.item._id.toString()
				}
			});
		}

		value.childs.forEach(
			function(e){
				me.addNode(e, value, node);
			}
		)

		return node;
	};


}

GraphPointController.prototype.calculation = function(id){
	var me=this;
	this.calculationModel.query({id:id}).$promise.then(
		function (res) {

			var elements = [];
			elements.push.apply(elements, me.addNode(res[0]));
			console.log(elements);
			var cy = cytoscape({
				zoom:0.8,
				container: document.getElementById("cy"), // container to render in
				elements:elements ,
				style: [ // the stylesheet for the graph
					{
						selector: "node",
						style: {
							'text-wrap': 'wrap',
							"background-color": "data(bg)",
							"label": "data(label)",
							"width": 12,
							"height": 12,
							"font-size":10

						}
					},

					{
						selector: "edge",
						style: {
							"width": 1,
							"line-color": "#ccc",
							"target-arrow-color": "#ccc",
							"target-arrow-shape": "triangle"
						}
					}
				],

				layout: me.options

			});
			cy.fit()
		},
		function (cancel) {
			console.log(cancel);
		});

}

GraphPointController.prototype.clientSearch =function (query){
	var me = this;
	return new Promise(function( success, failure){
		me.clientModel.query({name:query, lastName:query}).$promise.then(
		function(r){
			success(r._Array)
		},failure
		)
	})
}

GraphPointController.prototype.clientChange=function (cliente){
	console.log(cliente._id);
	this.calculation(cliente._id);
}


GraphPointController.prototype.test = function (e) {
	console.log(e);
};



