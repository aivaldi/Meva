app.controller("GraphPointAtController", ["$scope", "CalculationOrderFactory","ClientFactory", "$mdDialog", GraphPointAtController]);

function GraphPointAtController(_$scope, _calculation,_clientModel, _$mdDialog) {

	this.$scope = _$scope;
	this.calculationModel = _calculation;
	this.clientModel=_clientModel;
	this.$mdDialog = _$mdDialog;
	this.options = {
		name: "cola"//,
		//maxSimulationTime: 4000,
		//flow:{ axis: 'y', minSeparation: 50 },
		//fit: true
	};
	this.$scope.status={
		open:true
	}

	this.backgroudColorByStatus = function(status){
		switch (status){
		case 0:
			return "#efeca5";
		case 1:
			return "#188a11";
		case 2:
			return "#1d1adc";
		case 3:
			return "#AC5515";

		}
	};

	this.addNode=function(value, parent,_node)
	{
		var me =this;
		var node = [];
		if (_node)
			node=_node;
		var ret = {};
		ret["id"] = value.item._id.toString();
		ret["bg"] = this.backgroudColorByStatus(value.item.status);
		ret["label"] = "Nombre:"+value.item.name+((value.item.lastName)?","+value.item.lastName:"")+"\n"+
			((value.points!=undefined)?"Puntos:"+value.points+"\n":"")+
			((value.groupalPoints)?"Puntos grupales:"+value.groupalPoints+"\n":"")+
			((value.calculatedPoints)?"Puntos a favor:"+value.calculatedPoints:"");
		ret["client"] = value.item;
		ret["status"] = value.item.status;
		ret["points"] = value.points;

		node.push({data: ret});

		if (parent){
			node.push({
				data: {
					id: parent.item._id + value.item._id,
					source: parent.item._id.toString(),
					target: value.item._id.toString()
				}
			});
		}

		value.childs.forEach(
			function(e){
				me.addNode(e, value, node);
			}
		);

		return node;
	};


}


GraphPointAtController.prototype.calculationSave = function(dateFrom, dateTo){

	 ( new this.calculationModel({})).$save({id:0, dateFrom:dateFrom, dateTo:dateTo});
}

GraphPointAtController.prototype.calculation = function(dateFrom, dateTo){
	var me=this;
	this.calculationModel.query({id:0, dateFrom:dateFrom, dateTo:dateTo}).$promise.then(
		function (res) {

			var elements = [];
			elements.push.apply(elements, me.addNode(res[0]));
			console.log(elements);
			var cy = cytoscape({
				zoom:0.8,
				container: document.getElementById("cy"), // container to render in
				elements:elements ,
				style: [ // the stylesheet for the graph
					{
						selector: "node",
						style: {
							"text-wrap": "wrap",
							"background-color": "data(bg)",
							"label": "data(label)",
							"width": 12,
							"height": 12,
							"font-size":10

						}
					},

					{
						selector: "edge",
						style: {
							"width": 1,
							"line-color": "#ccc",
							"target-arrow-color": "#ccc",
							"target-arrow-shape": "triangle"
						}
					}
				],

				layout: me.options

			});
			cy.fit();
		},
		function (cancel) {
			console.log(cancel);
		});

};

GraphPointAtController.prototype.clientSearch =function (query){
	var me = this;
	return new Promise(function( success, failure){
		me.clientModel.query({name:query, lastName:query}).$promise.then(
			function(r){
				success(r._Array)
			},failure
		)
	})
}

GraphPointAtController.prototype.clientChange=function (dateFrom){

	if ( !dateFrom)
		return;
	var strDateFrom= new Date(dateFrom.getFullYear(),dateFrom.getMonth(),1).toISOString().slice(0,10).replace(/-/g,"");
	var strDateTo = new Date(dateFrom.getFullYear(),dateFrom.getMonth()+1,0).toISOString().slice(0,10).replace(/-/g,"");

	this.calculation(strDateFrom, strDateTo);
};


GraphPointAtController.prototype.saveCalculationPoints=function (dateFrom){

	if ( !dateFrom)
		return;
	var strDateFrom = new Date(dateFrom.getFullYear(),dateFrom.getMonth(),1).toISOString().slice(0,10).replace(/-/g,"");
	var strDateTo = new Date(dateFrom.getFullYear(),dateFrom.getMonth()+1,0).toISOString().slice(0,10).replace(/-/g,"");


	this.calculationSave(strDateFrom, strDateTo);
};


GraphPointAtController.prototype.test = function (e) {
	console.log(e);
};



