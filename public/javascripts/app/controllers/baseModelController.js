function BaseModelController(_$scope,_$mdDialog, _models, _errorService){

	this.$scope = _$scope;

	this.$mdDialog = _$mdDialog;

	this.modelsResource = _models;

	this.errorService = _errorService;

	this.init();

	this.list=true;
	this.listField=true;
	this.detail=false;

	this.showList = function(){
		this.list=true;
		this.detail=false;


	}

	this.showNewEdit = function(elem){
		this.list=false;
		this.detail=false;
		this.onEdit(elem);
	}

	this.showDetail = function(elem){
		this.list=false;
		this.detail=true;
	}

}

BaseModelController.prototype.test = function(e){
	console.log(e);
}

BaseModelController.prototype.showLoading = function(){
	this.loading=true;
}

BaseModelController.prototype.hideLoading = function(){
	this.loading=false;
}

/**
Es usado como evento para los que implemtan cuando se quiere editar algo
*/
BaseModelController.prototype.onEdit = function(element, secondElement){
}

BaseModelController.prototype.reset = function(){

var form = this.$scope.form_model;
    // Each control (input, select, textarea, etc) gets added as a property of the form.
    // The form has other built-in properties as well. However it's easy to filter those out,
    // because the Angular team has chosen to prefix each one with a dollar sign.
    // So, we just avoid those properties that begin with a dollar sign.
    let controlNames = Object.keys(form).filter(key => key.indexOf('$') !== 0);

    // Set each control back to undefined. This is the only way to clear validation messages.
    // Calling `form.$setPristine()` won't do it (even though you wish it would).
    for (let name of controlNames) {
        let control = form[name];
        control.$setViewValue(undefined);
    }

    form.$setPristine();
    form.$setUntouched();
}


BaseModelController.prototype.getQuery = function(searchTextModel){
	return {};
}

BaseModelController.prototype.loadData = function(page, pageSize,searchTextModel){
	var me=this;
	if (!page)
		page=1;

	if (!pageSize)
		pageSize=5;

	this.$scope.currentPage=page;
	this.$scope.pageSize=pageSize;


	this.$scope.models=[]
	this.$scope.modelFields=[]


	this.showLoading()
	var q={
		page:page,
		pageSize:pageSize
	}
	var queryField = this.getQuery(searchTextModel);
	if(queryField)
	{
		queryField['page']=page
		queryField['pageSize']=pageSize
		q=queryField;

	}
	this.modelsResource.query(q).$promise.then(
		function (ok){
			console.log(ok)
			me.$scope.models = ok._Array.map(function(e){ return new me.modelsResource(e) });
			me.$scope.pages = Math.ceil( ok.TotalCount/q.pageSize);
			if ( me.$scope.currentPage>=5 )
			{

				me.$scope.pageStart = me.$scope.currentPage-2;
			}
			else
				me.$scope.pageStart = 1;


			if (me.$scope.pageStart < 3)
				me.$scope.pageEnd=(5<=me.$scope.pages)?5:me.$scope.pages;

			else{

				var diff = me.$scope.pages - (me.$scope.currentPage+2);
				if (diff<0) {
					me.$scope.pageEnd = me.$scope.pages
					me.$scope.pageStart += diff
				}
				else{
						me.$scope.pageEnd = me.$scope.currentPage + 2
					}
				}



			me.$scope.pagesDisp = []
			for ( var i=me.$scope.pageStart;i<=me.$scope.pageEnd;i++)
				me.$scope.pagesDisp.push(i)

			me.hideLoading()
		},
		function (error){
			me.errorService(error);
			console.log("error");
			me.hideLoading()
		}
	)
}

BaseModelController.prototype.loadDataField = function(model){
	var me=this;
	this.$scope.modelFields=[]
	this.showLoading()
	this.fieldModelsResource.query({'id':model._id}).$promise.then(
		function (success){
			me.$scope.modelField=success
			me.showDetail();
			me.hideLoading()
		},
		function (error){
			me.errorService(error);
			console.log(error)
			me.hideLoading()
		}
	)
}

BaseModelController.prototype.init = function(){
	var me = this;
	this.loadData();


}
/**
Nuevo o edita
*/
BaseModelController.prototype.newOrEdit = function(model){
	if (model==null)
		this.$scope.selected = {}
	else
		this.$scope.selected = angular.copy(model)

	this.reset();
	this.showNewEdit(this.$scope.selected);
}


/**
Nuevo o edita
*/
BaseModelController.prototype.cancelNewOrEdit = function(){
	this.$scope.selected = null
	this.showList();
}

/**
Hace una llamada antes de guardar el objeto
*/
BaseModelController.prototype.onPreSave = function(elem){
	return elem;
}



/**
Nuevo o edita
*/
BaseModelController.prototype.save = function(){
	console.log('save')
	var me = this;
	(new this.modelsResource(
		this.onPreSave(this.$scope.selected)
		)).$save({id:this.$scope.selected._id}).then(
		function (success){
			console.log(success);
			me.$scope.selected = null
			me.loadData();
			me.showList();

		},
		function (error){
			me.errorService(error);
			console.log(error);
		}
	)

}

BaseModelController.prototype.delete = function(item){
	console.log('delete')
	var me = this;

	me.$mdDialog.show(
		me.$mdDialog.confirm()
			.title('Esta a punto de borrar un Modelo')
			.textContent("Esta seguo que quiere eliminar el registro " + item.NOMBRE)
			.ok("Borrar")
			.cancel("Cancelar")
		).then(
			function(success){
				item.$delete({id:item._id}).then(
						function (success){
							console.log(success);
							me.$scope.selected = null
							me.loadData();
							me.showList();

						},
						function (error){
							me.errorService(error);
							console.log(error);
						}
					)
			},
			function(no){}
		)



}





