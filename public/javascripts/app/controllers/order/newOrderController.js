function NewOrderController(_$scope,_$mdDialog, _clientModel,
	_stockModel,_orderFactory,_orderBillFactory,
	_errorService){
	var me = this;
	this.$scope = _$scope;

	this.$mdDialog = _$mdDialog;

	this.clientModel = _clientModel;
	this.stockModel = _stockModel;
	this.orderBillFactory = _orderBillFactory;
	this.orderFactory = _orderFactory;
	this.errorService = _errorService;
	this.elementEdit = false;
	this.$scope.$on("billitem-save",function(evt,product){
		me.okItemEdit(evt, product);
	} );
	this.$scope.$on("billitem-edit",function(evt, product){
		me.okItemEdit(evt, product);
	} );

	this.$scope.$on("billitem-delete",function(evt, idx){
		me.deleteItem(evt,idx);
	} );
	this.init();

}

NewOrderController.prototype.init = function(){
	var me = this;

	this.totalAdministrative = 10

	this.total=0;
	this.$scope.selected={};
	this.orderBillFactory.get({type:"oc"}).$promise.then(
		function(success){
			me.$scope.selected.number = success.number;
		},
		function(error){
			me.errorService(error);
		}
	);

	this.$scope.selected.date = new Date();
	this.$scope.selected.status="active";
	this.$scope.selected.products=[];

};

NewOrderController.prototype.clientSearch =function (query){
	console.log(query);
	var me = this;
	var queryParams ={
		name:query,
		lastName:query,
		dni:query
	}

	return new Promise(function(success,failure){
		me.clientModel.query(queryParams).$promise.then(function(ok){
			success(ok._Array)
		}, failure)
	})


};


NewOrderController.prototype.productSearch =function (query){
	console.log(query);
	var me = this;
	return new Promise(function(success,failure){
		me.productModel.query({name:query, lastName:query}).$promise.then(function(ok){
			success(ok._Array)
		}, failure)
	})

};

NewOrderController.prototype.addItem =function (){
	this.elementEdit=true;
	this.$scope.selected.products.push({ cant:1 });
};

NewOrderController.prototype.okItemEdit =function (evt, product){
	this.elementEdit=false;

	this.updateTotal()
};

NewOrderController.prototype.updateTotal = function (){
	this.total = Number(this.$scope.selected.products.reduce(
		function(x,y){
			return x+Number((y.sellPrice.toFixed(2)*y.cant).toFixed(2));
		},0
	).toFixed(2));
}


NewOrderController.prototype.cancelNewOrEdit = function (){
	var me = this;
	var alert = me.$mdDialog.alert({
		title: 'Orden de Compra',
		textContent: 'Cancelar orden de compra',
		ok: 'Aceptar'
	});

	me.$mdDialog
		.show( alert )
		.finally(function() {
			alert = undefined;
			me.init()
		});

}


NewOrderController.prototype.deleteItem =function (evt, idx){
	this.elementEdit=false;
	var me = this;
	me.$mdDialog.show(
		me.$mdDialog.confirm()
			.title("Esta a punto de borrar un Producto")
			.textContent("Esta seguo que quiere lo quiere eliminar?")
			.ok("Borrar")
			.cancel("Cancelar")
	).then(
		function(success){
			me.$scope.selected.products.splice(idx,1);
			this.updateTotal();
		},
		function(no){}
	);

};

/**
 * Valida los datos del formulario
 */
NewOrderController.prototype.validate = function(){
	this.updateTotal();
	this.$scope.selected.total = Number(this.total.toFixed(2));
	this.$scope.selected.totalPoints = Number(this.$scope.selected.client.calculatedPoints);
	this.$scope.selected.totalAdministrative = Number(this.totalAdministrative.toFixed(2));
	this.$scope.selected.totalToPaid = this.total - this.$scope.selected.client.calculatedPoints + this.$scope.selected.totalAdministrative  ;

	return true;
}

NewOrderController.prototype.save =function (){
	var me = this;
	me.$mdDialog.show(
		me.$mdDialog.confirm()
			.title("Desea generar la Orden de Compra?")
			.textContent("Una vez generada la orden no se puede modificar")
			.ok("Aceptar")
			.cancel("Cancelar")
	).then(
		function(success){
			if (me.validate())
				me.orderFactory.save(me.$scope.selected).$promise.then(
					function(ok){
						alert = me.$mdDialog.alert({
							title: 'Orden de Compra',
							textContent: 'Orden de compra generada correctamente',
							ok: 'Aceptar'
						});

						me.$mdDialog
							.show( alert )
							.finally(function() {
								alert = undefined;
								me.init()
							});
					},
					function(err){
						me.errorService(error);
					}
				);
		},
		function(no){}
	);
};

app.controller("NewOrderController",["$scope","$mdDialog", "ClientFactory","StockFactory","OrderFactory","OrderBillFactory","ErrorService",NewOrderController] );





