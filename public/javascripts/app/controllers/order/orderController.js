function OrderController(_$scope, _$mdDialog, _clientModel,
                         _stockModel, _orderFactory, _orderBillFactory,
                         _errorService) {
	var me = this;
	this.$scope = _$scope;

	this.$mdDialog = _$mdDialog;

	this.clientModel = _clientModel;
	this.stockModel = _stockModel;
	this.orderBillFactory = _orderBillFactory;
	this.orderFactory = _orderFactory;
	this.errorService = _errorService;
	this.orders = []
	this.list = true;
	this.init();

}

OrderController.prototype.init = function () {
	var me = this;

	this.loadData(1, 5);


};

OrderController.prototype.updateFilter = function (client, from, to, name, lastname) {
	var me = this;
	var query = {}
	if (client)
		query['client'] = client

	if (from) {
		query['dateFrom'] = from;
	}
	if (to) {
		var dateTo = new Date(to);
		dateTo.setDate(dateTo.getDate() + 1)
		query['dateTo'] = dateTo
	}

	if (name) {
		query['client.name'] = name
	}

	if (lastname) {
		query['client.lastName'] = lastname
	}


	this.orderFactory.queryInfo(query).$promise.then(
		function (success) {
			me.orders = success._Array;
		},
		function (error) {
			me.errorService(error);
		}
	);
};

OrderController.prototype.show = function (client) {
	this.$scope.selected = client;
	this.list = false;
}


OrderController.prototype.loadData = function (page, pageSize, client, from, to, name, lastname) {
	var me = this;
	if (!page)
		page = 1;

	if (!pageSize)
		pageSize = 5;

	this.$scope.currentPage = page;
	this.$scope.pageSize = pageSize;


	this.$scope.models = []


	var q = {
		page: page,
		pageSize: pageSize
	}

	var query = {}
	if (client)
		query['client'] = client

	if (from) {
		query['dateFrom'] = from;
	}
	if (to) {
		var dateTo = new Date(to);
		dateTo.setDate(dateTo.getDate() + 1)
		query['dateTo'] = dateTo
	}

	if (name) {
		query['client.name'] = name
	}

	if (lastname) {
		query['client.lastName'] = lastname
	}

	var queryField = query


	if (queryField) {
		queryField['page'] = page
		queryField['pageSize'] = pageSize
		q = queryField;

	}
	this.orderFactory.queryInfo(query).$promise.then(
		function (ok) {
			console.log(ok)
			me.orders = ok._Array;
			me.$scope.pages = Math.ceil(ok.TotalCount / q.pageSize);
			if (me.$scope.currentPage >= 5) {

				me.$scope.pageStart = me.$scope.currentPage - 2;
			}
			else
				me.$scope.pageStart = 1;


			if (me.$scope.pageStart < 3)
				me.$scope.pageEnd = (5 <= me.$scope.pages) ? 5 : me.$scope.pages;

			else {

				var diff = me.$scope.pages - (me.$scope.currentPage + 2);
				if (diff < 0) {
					me.$scope.pageEnd = me.$scope.pages
					me.$scope.pageStart += diff
				}
				else {
					me.$scope.pageEnd = me.$scope.currentPage + 2
				}
			}


			me.$scope.pagesDisp = []
			for (var i = me.$scope.pageStart; i <= me.$scope.pageEnd; i++)
				me.$scope.pagesDisp.push(i)


		},
		function (error) {
			me.errorService(error);
			console.log("error");

		}
	)

	this.saveToPDF = function (oc) {
		var doc = new jsPDF('p', 'pt');
		var posX = 40;
		var posY = 60;

		doc.setFont("helvetica");
		doc.setFontSize(18);

		doc.text(posX, posY, "MEVA");

		var date = new Date();
		doc.text(posX+380, posY, "Fecha "+date.getDate()+'/'+date.getMonth() +'/'+date.getFullYear()  );
		posY += 40
		doc.setFontSize(16);

		date = new Date(oc.date);
		doc.text(posX, posY, "Remito de Orden de Compra N: " + oc.number)
		posY += 40
		doc.text(posX, posY, "Fecha Orden de Compra: "+date.getDate()+'/'+date.getMonth() +'/'+date.getFullYear() );
		posY += 40
		doc.text(posX, posY, "Cliente: " + oc.client.lastName + ", " + oc.client.name )
		doc.setFontSize(12);
		posY += 40
		doc.text(posX, posY, "DNI:" + oc.client.dni);
		posY += 40
		doc.text(posX, posY, "Dirección: " + oc.client.street + " " + oc.client.number + "(" + oc.client.zip + ")");
		posY += 20
		doc.text(posX, posY, "Provincia: " + oc.client.province + "  Ciudad:" + oc.client.city);
		doc.setFontSize(9);
		posY += 20
		doc.text(posX, posY, "Nombre Producto");
		doc.text(posX + 300, posY, "Precio ");
		doc.text(posX + 400, posY, "Cantidad ");
		doc.text(posX + 500, posY, "Total ");
		doc.line(posX, posY+7, posX+580, posY+7);

		oc.products.forEach(function (p) {
			posY += 20
			doc.text(posX, posY, (p.productName)?p.productName:'');
			doc.text(posX + 300, posY, '$'+p.sellPrice.toFixed(2).toString());
			doc.text(posX + 400, posY, '$'+p.cant.toFixed(2).toString());
			doc.text(posX + 500, posY, '$'+(p.sellPrice * p.cant).toFixed(2).toString());
			doc.line(posX, posY+7, posX+580, posY+7);

		})
		posY += 50
		doc.text(posX, posY, "Total : $" + oc.total.toFixed(2).toString());

		posY += 20
		doc.text(posX, posY, "Gasto administrativo : $" + oc.totalAdministrative.toFixed(2).toString());

		posY += 20
		doc.text(posX, posY, "Puntos usados : $" + oc.totalPoints.toFixed(2).toString());

		posY += 20
		doc.text(posX, posY, "Total a pagar : $" + oc.totalToPaid.toFixed(2).toString());


		doc.save(oc.client.lastName + oc.client.name + oc.number + ".pdf");


	}
}

app.controller("OrderController", ["$scope", "$mdDialog", "ClientFactory", "StockFactory", "OrderFactory", "OrderBillFactory", "ErrorService", OrderController]);





