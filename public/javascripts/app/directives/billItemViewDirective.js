app.directive("billItemView", function(){

	return {

		restrict:"EA",
		scope:{item:"=", idx:"=",selectedProducts:"=", noEdit:"="},
		controllerAs:"ctr",
		controller : ["$scope","$q","BrandFactory","ProductFactory","StockFactory",
			function billingItemController(_$scope,_$q, _brandFactory, _productFactory,_stockFactory ){
				console.log("controller billitem created");
				this.$scope = _$scope;
				this.$q = _$q;
				this.brandFactory = _brandFactory;
				this.productFactory = _productFactory;
				this.stockFactory = _stockFactory;
				if (this.$scope.selectedProducts)
					this.selectedProductsId = this.$scope.selectedProducts.filter(function(e){ return e.product!=undefined;}).map(function(p){return p.product._id.toString();});
				else
					this.selectedProductsId=[];
				this.mode="edit";
        
				this.isEdit=(this.$scope.item._id)?false:true;


				this.brand = null;
				this.product= null;

				if (this.$scope.item.product) {
					this.brand = this.$scope.item.product.brand || null;
					this.product= this.$scope.item.product ||null;

					this.$scope.item.productName = this.$scope.item.product.name
					this.$scope.item.brandName = this.$scope.item.product.brand.name
				}

				this.searchBrand = function(query){
					console.log(query);
					return this.brandFactory.query({name:query, lastName:query}).$promise;
				};

				this.searchProduct = function(query){
					var me = this;
					console.log(query);
					return me.$q( function(success, error){
						me.productFactory.query({name:query, lastName:query, code:query}).$promise.then(
							function(values){
								success(
									values._Array.filter(function(val){  return $.inArray(val._id.toString(),me.selectedProductsId)==-1;  })
								);
							},
							error
						);

					});


				};

				this.brandChanged = function(){
					this.product=null;
					this.$scope.item.brandId=this.brand._id;
					this.$scope.item.brandName=this.brand.name;
				};

				this.productChanged = function(){
					var me = this;
					if (this.product==null || this.product==undefined)
					{
						this.$scope.item.productId=null;
						this.$scope.item.productName=null;
						this.$scope.item.price = null;
						this.$scope.item.points = null; 
                
					}    
					this.$scope.item.product=this.product;
					this.$scope.item.productName=this.product.name +' '+this.product.brand.name + ' '+this.product.code;
					this.stockFactory.query({product_id:this.product._id.toString()}).$promise.then(
						function (res){
							if (res.length==0)
								return;
							res = res._Array[0];
							console.log(res);
							me.stock=res;
							me.$scope.item.sellPrice = res.sellPrice;
							me.$scope.item.price = res.price;
							me.$scope.item.points = res.points; 
							me.productsLeft = res.cantProducts;
							console.log(me.$scope.item);
                    
						},
						function(error){
							console.log(error);
						}
					);
				};

				this.save = function(){
					console.log(this.$scope.item);

					this.isEdit=false;

					this.$scope.$emit("billitem-save");

				};

				this.toEdit = function(){
					var me = this;
					if (this.$scope.selectedProducts)
						this.selectedProductsId = this.$scope.selectedProducts.filter(
							function(e){
								return ( e.product!=undefined && e.product._id!==me.$scope.item.product._id);
							}
						).map(function(p){return p.product._id.toString();});


					else
						this.selectedProductsId=[];

					this.isEdit = true;
					this.$scope.$emit("billitem-edit");
				};
				this.toDelete = function(){
					this.isEdit = false;
					this.$scope.$emit("billitem-delete", this.$scope.idx);
				};


			}],
		templateUrl:"/partials/directive/billing/item-view.html"

	};

});