app.directive("billItem", function(){

	return {

		restrict:"EA",
		scope:{item:"=", idx:"=",selectedProducts:"=", noEdit:"="},
		controllerAs:"ctr",
		controller : ["$scope","$q","BrandFactory","ProductFactory","StockFactory",
			function billingItemController(_$scope,_$q, _brandFactory, _productFactory,_stockFactory ){
				console.log("controller billitem created");
				this.$scope = _$scope;
				this.$q = _$q;
				this.brandFactory = _brandFactory;
				this.productFactory = _productFactory;
				this.stockFactory = _stockFactory;
				if (this.$scope.selectedProducts)
					this.selectedProductsId = this.$scope.selectedProducts.filter(function(e){ return e.product!=undefined;}).map(function(p){return p.product._id.toString();});
				else
					this.selectedProductsId=[];
				this.mode="edit";
        
				this.isEdit=(this.$scope.item._id)?false:true;


				this.brand = null;
				this.product= null;

				if (this.$scope.item.product) {
					this.brand = this.$scope.item.product.brand || null;
					this.product= this.$scope.item.product ||null;

					this.$scope.item.productName = this.$scope.item.product.name
					this.$scope.item.brandName = this.$scope.item.product.brand.name
				}

				this.searchBrand = function(query){
					console.log(query);
					return this.brandFactory.query({name:query, lastName:query}).$promise;
				};

				this.searchProduct = function(query){
					var me = this;
					console.log(query);
					return me.$q( function(success, error){
						me.stockFactory.queryAll({name:query}).$promise.then(
							function(values){
								success(
									values._Array.filter(function(val){  return $.inArray(val._id.toString(),me.selectedProductsId)==-1;  })
								);
							},
							error
						);

					});


				};



				this.productChanged = function(){
					var me = this;
					if (this.product==null || this.product==undefined)
					{
						this.$scope.item.productId=null;
						this.$scope.item.productName=null;
						this.$scope.item.price = null;
						this.$scope.item.points = null; 
                
					}    
					this.$scope.item.product=this.product.product;
					this.$scope.item.productName=this.product.product.name +' '+this.product.product.brand.name + ' '+this.product.product.code;

					me.stock=this.product;
					me.$scope.item.sellPrice = this.product.sellPrice;
					me.$scope.item.price = this.product.price;
					me.$scope.item.points = this.product.points;
					me.productsLeft = this.product.cantProducts;
					
				};

				this.save = function(){
					console.log(this.$scope.item);

					this.isEdit=false;

					this.$scope.$emit("billitem-save");

				};

				this.toEdit = function(){
					var me = this;
					if (this.$scope.selectedProducts)
						this.selectedProductsId = this.$scope.selectedProducts.filter(
							function(e){
								return ( e.product!=undefined && e.product._id!==me.$scope.item.product._id);
							}
						).map(function(p){return p.product._id.toString();});


					else
						this.selectedProductsId=[];

					this.isEdit = true;
					this.$scope.$emit("billitem-edit");
				};
				this.toDelete = function(){
					this.isEdit = false;
					this.$scope.$emit("billitem-delete", this.$scope.idx);
				};


			}],
		templateUrl:"/partials/directive/billing/item.html"

	};

});