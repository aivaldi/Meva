app.factory('ProductFactory', ['$resource', function ($resource) {
	return $resource(apiURL + '/product/:id' + '/:child', null, {
			query: {
				method: 'GET',
				isArray: false,
				transformResponse: function (data) {
					if (angular.fromJson(data).Result == 'Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}
			}

		}
	);
}]);