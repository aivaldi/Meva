app.factory('CalculationFactory', ['$resource', function($resource) {
	return $resource('api/1/graph/client/status/:id', null,{
			query: {
				method: 'GET',
				isArray: true,
				transformResponse: function(data) {
					if (angular.fromJson(data).Result=='Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}
			}
		}
	);
}]);

app.factory('CalculationOrderFactory', ['$resource', function($resource) {
	return $resource('api/1/graph/client/status/:id/order/:dateFrom/:dateTo', null,{
			query: {
				method: 'GET',
				isArray: true,
				transformResponse: function(data) {
					if (angular.fromJson(data).Result=='Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}
			}
		}
	);
}]);