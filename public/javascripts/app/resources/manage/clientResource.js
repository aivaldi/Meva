app.factory('ClientFactory', ['$resource', function($resource) {
	return $resource(apiURL+'/client/:id' + '/:child', null,{
		query: {
		      method: 'GET',
		      isArray: false,
		      transformResponse: function(data) {
		      	if (angular.fromJson(data).Result=='Success')
		        	return angular.fromJson(data).value;
		        else
		        	return angular.fromJson(data)
		      }
		    },
			getLineNumber:{
				url:apiURL+'/client/line/:line',
				method: 'GET',
				isArray: false

			},

			getChildsOrders:{
				url:apiURL+'/client/:id/childs/orders/:date   ',
				method: 'GET',
				isArray: true,
				transformResponse: function(data) {
					if (angular.fromJson(data).Result=='Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}

			},

			getByBirthday:{
				url:apiURL+'/client/birthday/:date   ',
				method: 'GET',
				isArray: true,
				transformResponse: function(data) {
					if (angular.fromJson(data).Result=='Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}
			},
			getClientUntil:{
				url:apiURL+'/client/until/:period/:from/:to',
				method: 'GET',
				isArray: true,
				transformResponse: function(data) {
					if (angular.fromJson(data).Result=='Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}
			}


		}
		);
}]);