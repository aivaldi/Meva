app.factory('OrderFactory', ['$resource', function($resource) {
	return $resource(apiURL+'/order/:id' + '/:child', null,{
			query: {
				method: 'GET',
				isArray: true,
				transformResponse: function(data) {
					if (angular.fromJson(data).Result=='Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}
			},
			queryInfo: {
				url:apiURL+'/order/info',
				method: 'GET',
				isArray: false,
				transformResponse: function(data) {
					if (angular.fromJson(data).Result=='Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}
			},
			queryAll: {
				url:apiURL+'/stock/all',
				method: 'GET',
				isArray: false,
				transformResponse: function(data) {
					if (angular.fromJson(data).Result=='Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}
			},

			
		
		info: {
		      method: 'GET',
		      isArray: false,
		      params:{ child:'info'}
		    }
		}
		);
}]);

app.factory('OrderBillFactory', ['$resource', function($resource) {
	return $resource(apiURL+'/order/:type/next', null,{
		query: {
		      method: 'GET',
		      isArray: true,
		      transformResponse: function(data) {
		      	if (angular.fromJson(data).Result=='Success')
		        	return angular.fromJson(data).value;
		        else
		        	return angular.fromJson(data)
		      }
		    },
		
		info: {
		      method: 'GET',
		      isArray: false,
		      params:{ child:'info'}
		    }
		}
		);
}]);