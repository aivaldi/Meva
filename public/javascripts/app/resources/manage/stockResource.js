app.factory('StockFactory', ['$resource', function ($resource) {
	return $resource(apiURL + '/stock/:id' + '/:child', null, {
			query: {
				method: 'GET',
				isArray: false,
				transformResponse: function (data) {
					if (angular.fromJson(data).Result == 'Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}
			},
			queryBrand: {
				url: apiURL + '/stock/info/:brand',
				method: 'GET',
				isArray: true,
				transformResponse: function (data) {
					if (angular.fromJson(data).Result == 'Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}


			},
			queryAll: {
				url: apiURL + '/stock/all',
				method: 'GET',
				isArray: false,
				transformResponse: function (data) {
					if (angular.fromJson(data).Result == 'Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}
			},
			updateValue: {
				url: apiURL + '/stock/update',
				method: 'POST'

			}
		}
	);
}]);

app.factory('ProductSellFactory', ['$resource', function ($resource) {
	return $resource(apiURL + '/info/products/sell/:from/:to', null, {
			query: {
				method: 'GET',
				isArray: true,
				transformResponse: function (data) {
					if (angular.fromJson(data).Result == 'Success')
						return angular.fromJson(data).value;
					else
						return angular.fromJson(data)
				}
			},
			queryBrand: {
				url: apiURL + '/info/brand/:id/sell/:from/:to',
				method: 'GET',
				isArray: false

			}
		}
	);
}]);