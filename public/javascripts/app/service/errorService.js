app.factory('ErrorService', ['$mdToast', function($mdToast) {
   return function(msg) {
   	//si se manda un objeto saco los datos de message
   	 if (msg.data instanceof Object)
     {
	     if (msg.data.errmsg)
		     msg = msg.data.errmsg
	     else
	     	msg = msg.data.message
     }
     else
      if (msg.status!=200)
        msg = msg.statusText;
     $mdToast.show({
     	template: '<md-toast class="md-toast" >' + msg + '</md-toast>',
     	 hideDelay: 6000,
        position: 'bottom right'
     })
   };
 }]);