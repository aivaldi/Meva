var bcrypt = require('bcrypt');
var mongoose = require('mongoose');

var mod = LoginService.prototype;

var User = mongoose.model('User');  

function LoginService(user, password){
	this._user = user;
	this._password = password;
} 

/**
Trata de autenticar con el usuario y password que tiene
*/
mod.authenticate = function () {
	var me = this;
	return new Promise(function(fulfill, reject){
		User.authenticate(me._user, me._password).done(
	    	function(user){
	    		console.log ("login encontrado")
	    		var randomNumber=Math.random().toString();
			    randomNumber=randomNumber.substring(2,randomNumber.length);
				user.updateToken( randomNumber ).done(
					function (user){
						console.log("authenticated!!", user)
						fulfill(user);
					},
					function (error){
						reject(error)
					}
				)
			}
			,
			function(error){
				console.log (error);
				reject(error)
			}
	    )
		
    });
	
}

module.exports = LoginService;