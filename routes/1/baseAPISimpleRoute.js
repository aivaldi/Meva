var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var passport = require('passport');

var resultDecorator = require('../../resultDecorator.js')

var audit = require('../../model/audit.js')
/**
 * Options permite agregar el pre decada accion
 * preSave
 * preDelete
 * preUpdate
 * @param urlBase
 * @param model
 * @param extend
 * @param overrides
 * @param options
 * @returns {*}
 */
module.exports = function (urlBase, model, extend, overrides, options) {

	if (overrides == null || overrides == undefined)
		overrides = {}

	if (extend != null || extend != undefined)
		extend(router);

	if (!options)
		options = {}

	/* GET  listing. */
	if (overrides.getAll)
		overrides.getAll(router)
	else
		router.get(urlBase, passport.authenticate('jwt', {session: false}), function (req, res, next) {

			var q = []
			var qAnd = []

			Object.keys(req.query).forEach(
				function (key) {
					var r = {}
					if (key.substr(key.length - 3, 3) === "_id") {
						key = key.substr(0, key.length - 3)
						r[key] = mongoose.Types.ObjectId(req.query[key + "_id"].toString())
						qAnd.push(r)
					} else {
						if (req.query[key] != '' && model.schema.paths[key]) {
							if (model.schema.paths[key].instance=='Number') {
								if (!isNaN(req.query[key]))
								{
									r[key] = req.query[key]
									q.push(r)
								}
							}else
							{
								r[key] = new RegExp(req.query[key].replace(' ','.*'), 'i')
								q.push(r)
							}

						}
					}
				}
			)

			var query = {}
			if (qAnd.length > 0) {
				if (q.length > 0)
					qAnd.push({$or: q})
				query = {'$and': qAnd}
			}
			else if (q.length > 0)
				query = {$or: q}


			var modelToFind = model.find(query);
			if (options.hasPopulate)
			{
				modelToFind = options.populate(modelToFind)
			}


			var _pageNumber = (req.query.page)?Number(req.query.page):0,
				_pageSize = (req.query.pageSize)?Number(req.query.pageSize):5;

			model.count(query,function(err,count){

				modelToFind.skip(_pageNumber > 0 ? ((_pageNumber - 1) * _pageSize) : 0).limit(_pageSize).exec(function(err, docs) {
					if (err)
						res.json(err);
					else
						res.json({
							"Result":"Ok",
							"TotalCount": count,
							"TotalPage": docs.length,
							"_Array": docs
						});
				});
			});





		});

	/** GET  by id **/
	router.get(urlBase + '/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {

		model.findOne({_id: req.params.id}).exec().then(
			function (ok) {
				console.log(ok);
				res.send(ok);
			},
			function (error) {
				console.log(error);
				res.status(500).send('Something broke!');
			}
		)

	});


	/** new */
	if (options.preSave)

		router.post(urlBase, passport.authenticate('jwt', {session: false}), function (req, res, next) {
			options.preSave(req, res, next);
		});

	router.post(urlBase, passport.authenticate('jwt', {session: false}), function (req, res, next) {


		req.body['audit'] = audit.newAudit(req.user.login)
		var m = new model(
			req.body
		)
		m.save(function (err, small) {
			if (err) {
				console.log(err);
				res.status(500).send(err);
				return;
			}
			res.send(small);
			next();
		})

	});

	/** update */
	/** new */
	if (options.preUpdate)

		router.post(urlBase + '/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {
			options.preUpdate(req, res, next);
		});
	router.post(urlBase + '/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {
		var m = req.body;
		req.body['audit'] = audit.updateAudit(m.audit, req.user.login)
		console.log(req.params.id, m)
		delete m['_id']
		model.findByIdAndUpdate(req.params.id, {$set: m}, {new: true}, function (err, small) {
			if (err) {
				console.log(err);
				res.status(500).send(err);
				return;
			}
			if (small == null) {

				console.log("update record not found on " + urlBase);
				res.status(500).send("No se encoontro el registro solicitado " + urlBase);
				return;
			}
			console.log(small)
			res.send(small);
		})

	});

	/** delete */
	/** update */
	/** new */
	if (options.preDelete)

		router.delete(urlBase + '/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {
			options.preDelete(req, res, next);
		});

	router.delete(urlBase + '/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {

		model.remove({_id: req.params.id}, function (err, small) {
			if (err) {
				console.log(err);
				res.status(500).send(err);

				return;
			}
			res.send(small);
		})
	});


	return router;
}
