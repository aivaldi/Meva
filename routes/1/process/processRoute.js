var express = require("express");
var router = express.Router();
var passport = require("passport");

var resultDecorator = require("../../../resultDecorator.js");
var OrderPointsCalculation = require("../../../calculation/execution.js");
var Client = require("../../../model/client.js");
var CalculationStatus = require("../../../model/calculationStatus.js");

/** 
  Calcular doy de alta un periodo
*/
router.get("/calculate",passport.authenticate("jwt", { session: false}), function(req, res, next) {
	var pc = new OrderPointsCalculation();
	pc.calculate().then(
		function(ok){
			res.send(ok);
		},
		function(error){
			res.status(500);
			res.send(error);
		}
	);


});

/**
 Calcular doy de alta un periodo
 */
router.get("/graph/client/:id",passport.authenticate("jwt", { session: false}), function(req, res, next) {
	var client = new Client();
	Client.getGraph(req.params.id).then(
		function(ok){
			res.send(ok);
		},
		function(error){
			res.status(500);
			res.send(error);
		}
	);


});


/**
 Calcular doy de alta un periodo
 */
router.get("/graph/client/status/:id",passport.authenticate("jwt", { session: false}), function(req, res, next) {
	var client = new Client();
	Client.getGraphStatus(req.params.id).then(
		function(ok){
			res.send(ok);
		},
		function(error){
			res.status(500);
			res.send(error);
		}
	);


});

/**
 Calcular doy de alta un periodo
 */
router.get("/graph/client/status/:id/order/:from/:to",passport.authenticate("jwt", { session: false}), function(req, res, next) {
	var pc = new OrderPointsCalculation();
	var dateSplit = function (dateStr) {
		var ret = [];
		ret.push( dateStr.substr(0,4) );
		ret.push( dateStr.substr(4,2)-1 );
		ret.push( dateStr.substr(6,2) );
		return ret;
	};
	var fromSplit = dateSplit (req.params.from);
	var toSplit = dateSplit(req.params.to);
	console.log(toSplit[0],toSplit[1],toSplit[2])
	console.log("***** " + (new Date(toSplit[0],toSplit[1],toSplit[2] )))
	pc.calculate(
		new Date(toSplit[0],toSplit[1],toSplit[2] ) ,
		new Date(fromSplit[0],fromSplit[1],fromSplit[2] ) ).then(
		function(ok){
			Client.getGraphStatusWithOrder(req.params.id, ok).then(
				function(ok){
					res.send(ok);
				},
				function(error){
					res.status(500);
					res.send(error);
				}
			);
		},
		function(error){
			res.status(500);
			res.send(error);
		}
	);




});

router.post("/graph/client/status/:id/order/:form/:to",passport.authenticate("jwt", { session: false}), function(req, res, next) {
	var pc = new OrderPointsCalculation();
	var dateSplit = function (dateStr) {
		var ret = [];
		ret.push( dateStr.substr(0,4) );
		ret.push( dateStr.substr(4,2)-1 );
		ret.push( dateStr.substr(6,2) );
		return ret;
	};
	var fromSplit = dateSplit (req.params.form);
	var toSplit = dateSplit(req.params.to);
	var dateTo=new Date(toSplit[0],toSplit[1],toSplit[2] );
	var dateFrom=new Date(fromSplit[0],fromSplit[1],fromSplit[2] ,23,59,59);
	console.log("***** " + dateTo)
	pc.calculate(
		 dateTo, dateFrom
		 ).then(
		function(ok){
			Client.getGraphStatusWithOrder(req.params.id, ok).then(
				function(nodes){

					Client.saveGraphStatusWithOrder(nodes);
					var cs = new CalculationStatus(
						{to:dateTo,from:dateFrom});

					cs.save(function(err){
						console.log(err);
					});
					res.send(nodes);

				},
				function(error){
					res.status(500);
					res.send(error);
				}
			);
		},
		function(error){
			res.status(500);
			res.send(error);
		}
	);

});

module.exports = router;