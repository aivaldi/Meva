var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var model = require('../../../model/product.js')
var modelStock = require('../../../model/stock.js')
var modelOrder = require('../../../model/order.js')


var extend = function (router) {
	router.get("/product/:id/info", passport.authenticate('jwt', {session: false}), function (req, res, next) {

		model.findOne({_id: req.params.id}).populate('brand').exec().then(
			function (ok) {
				console.log(ok);
				res.send(ok);
			},
			function (error) {
				console.log(error);
				console.log('error');
			}
		)
	})

}

var routeBase = require('../baseAPISimpleRoute.js')('/product', model, extend,{},{

	hasPopulate:true,
	populate: function(model){
		return model.populate('brand');
	},

	preDelete : function(req, res, next){
		modelStock.findOne({product:req.params.id}).exec().then(
			function(ok){
				console.log('preDelte', ok);
				if (ok==null)
					modelOrder.findOne({'products.product':(new mongoose.Types.ObjectId(req.params.id)) }).exec().then(
						function(ok){
							console.log('preDelte', ok);
							if (ok==null)
								next();
							else
								res.status(500).send({status:'error', message:'No puede borrar un producto con orden asociado' });
						},
						function (error) {
							console.log(error);
							console.log('error');
						}

					);
				else
					res.status(500).send({status:'error', message:'No puede borrar un producto con stock asociado' });
			},
			function (error) {
				console.log(error);
				console.log('error');
			}

		)
	}
	,
preSave:function(req, res, next){
		console.log('save', req.body.code);
		model.findOne({code:req.body.code}).exec().then(
			function (ok) {

				console.log('save', ok);
				if (ok==null)
					next();
				else
					res.status(500).send({status:'error', message:'Codigo de producto existente' });
			},
			function (error) {
				console.log(error);
				console.log('error');
			}
		)
	},
	preUpdate:function(req, res, next){
		console.log('update', req.body.code, req.body._id.toString());
		model.findOne({_id:{$ne:req.body._id.toString()}, code:req.body.code}).exec().then(
			function (ok) {

				console.log('update', ok);
				if (ok==null)
					next();
				else
					res.status(500).send({status:'error', message:'Codigo de producto existente' });
			},
			function (error) {
				console.log(error);
				console.log('error');
			}
		)
	}
})


module.exports = routeBase;
