var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var passport = require('passport');
var clientModel = require('../../../model/client.js')
var orderModel = require('../../../model/order.js')

var extend = function (router) {
	router.get("/client/line/:line", passport.authenticate('jwt', {session: false}), function (req, res, next) {
		console.log(req.params.line);
		if (!req.params.line) {
			return;
		}
		clientModel.findOne({line: req.params.line}).sort('-codeNumber').exec().then(
			function (ok) {
				if (ok) {
					if (ok.codeNumber != undefined)
						res.send({value: ok.codeNumber + 1});
					else
						res.send({value: 0});
				} else
					res.send({value: 0});
			},
			function (error) {
				console.log(error);
				console.log('error');
			}
		)
	})


	router.get("/client/until/:period/:from/:to", passport.authenticate('jwt', {session: false}), function (req, res, next) {

		var timeRestriction =
			[
				{dateFilter: {$gte: req.params.from}},
				{dateFilter: {$lte: req.params.to}}
			]

		var format = ''
		switch (req.params.period) {
			case 'año':
				format = '%Y'
				break;
			case 'mes':
				format = '%Y%m'
				break;
			case 'dia':
				format = '%Y%m%d'
				break;
		}

		console.log(timeRestriction)
		clientModel.aggregate(
			[
				{
					$project: {
						_id: 1,
						birthday: 1,
						name: 1,
						lastName: 1,
						line: 1,
						codeNumber: 1,
						dni: 1,
						dateString: {'$dateToString': {format: format, date: "$audit.created"}},
						dateFilter: {'$dateToString': {format: "%Y%m%d", date: "$audit.created"}}
					}
				},
				{
					$match: {$and: timeRestriction}
				},
				{
					$group: {
						_id: {date: '$dateString'},
						cant: {$sum: 1}
					}
				},
				{
					$project: {
						date: "$_id.date",
						cant: 1
					}
				}])
			.exec().then(
			function (r) {
				res.json(r);
			},
			function (err) {
				console.log(err)

			}
		)

	})

	router.get("/client/birthday/:month", passport.authenticate('jwt', {session: false}), function (req, res, next) {

		var month = req.params.month;

		console.log(month);
		clientModel.aggregate([

			{$match: {birthday: {'$ne': null}}},
			{
				$project: {
					_id: 1,
					birthday: 1,
					name: 1,
					lastName: 1,
					line: 1,
					codeNumber: 1,
					dni: 1,
					month: {$month: '$birthday'}
				}
			},
			{$match: {month: {'$eq': Number(month)}}},
			{
				$project: {
					_id: 1,
					birthday: 1,
					name: 1,
					lastName: 1,
					line: 1,
					codeNumber: 1,
					dni: 1

				}
			}

		])
			.exec().then(
			function (r) {
				res.json(r);
			},
			function (err) {
				console.log(err)

			}
		)
	})

	router.get("/client/:id/childs/orders/:date", passport.authenticate('jwt', {session: false}), function (req, res, next) {


		var dateSplit = function (dateStr) {
			var ret = [];
			ret.push(dateStr.substr(0, 4));
			ret.push(dateStr.substr(4, 2) - 1);
			ret.push(dateStr.substr(6, 2));
			return ret;
		};
		var fromSplit = dateSplit(req.params.date);
		var dateFrom = new Date(fromSplit[0], fromSplit[1], fromSplit[2], 0, 0, 0);

		var strDateFrom = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), 1, 0, 0, 0)
		var strDateTo = new Date(dateFrom.getFullYear(), dateFrom.getMonth() + 1, 0, 23, 59, 59)


		var dateFilter = {
			$lte: strDateTo,
			$gte: strDateFrom
		}
		console.log(dateFilter)
		clientModel.find({parent: req.params.id}).exec().then(
			function (clients) {
				var idClients = clients.map(function (v) {
					return v._id;
				})
				orderModel.find({date: dateFilter, client: {$in: idClients}}).populate('client')
					.populate(
						{
							path: 'products.product',
							populate: {
								path: "brand",
								model: "ProductBrandSchema"
							}
						}
					)
					.exec().then(
					function (r) {

						var response = r.reduce(
							function (x, y) {
								var cid = y.client._id;
								if (!x[cid]) {
									x[cid] = {
										client: y.client,
										orders: []
									}
								}
								y['client'] = undefined;
								x[cid].orders.push(y);
								return x;
							}
							, {});
						res.json(Object.keys(response).map(function (k) {
							return response[k]
						}))
					},
					function (error) {
						console.log(error)
					}
				)
			},
			function (error) {
				console.log(error)
			}
		)

	})

}

var routeBase = require('../baseAPISimpleRoute.js')('/client', clientModel, extend, null, {
	preDelete: function (req, res, next) {
		clientModel.find({parent: req.params.id}).exec().then(
			function (r) {
				r.forEach(function (j) {
					j.parent = j.parent.filter(function (v) {
						return v != req.params.id;
					})
					j.save();
				});
				next()
			},
			function (err) {
				console.log(err);
				console.log('error');
			}
		)
	}
})


module.exports = routeBase;
