var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var passport = require('passport');
var model = require('../../../model/orderBill.js')

var resultDecorator = require('../../../resultDecorator.js')

/** GET  by id **/
router.get('/order/:type/next',passport.authenticate('jwt', { session: false}), function(req, res, next) {
  	  
      model.findOne({type:req.params.type}).exec().then (
      function(ok){
        console.log (ok);
        res.send(ok);   
      },
      function(error){
        console.log (error);
        res.status(500).send('Something broke!');
      }
    )

});

module.exports = router 
