var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var passport = require('passport');
var model = require('../../../model/productBrand.js')
var modelProduct = require('../../../model/product.js')

var routeBase = require('../baseAPISimpleRoute.js')('/brand',model ,null,{},{
	preDelete : function(req, res, next){
		modelProduct.findOne({brand:req.params.id}).exec().then(
			function(ok){
				console.log('preDelte', ok);
				if (ok==null)
					next();
				else
					res.status(500).send({status:'error', message:'No puede borrar una marca con producto asociado' });
			},
			function (error) {
				console.log(error);
				console.log('error');
			}

		)
	}
})


module.exports = routeBase;
