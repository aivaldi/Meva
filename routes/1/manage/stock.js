var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var passport = require('passport');
var model = require('../../../model/stock.js')
var modelProduct = require('../../../model/product.js')
var modelOrder = require('../../../model/order.js')

var extend = function(router){
	router.get("/stock/:id/info",passport.authenticate('jwt', { session: false}), function(req, res, next) {

		model.findOne({_id:req.params.id}).populate('product').exec().then (
			function(ok){
				console.log (ok);
				res.send(ok);
			},
			function(error){
				console.log (error);
				console.log ('error');
			}
		)
	})

	router.get("/stock/info/:brand",passport.authenticate('jwt', { session: false}), function(req, res, next) {

		model.find().populate({
			path: 'product',
			populate: {
				path: 'brand',
				model: 'ProductBrandSchema'
			}
		}).exec().then (
			function(r){
				var ok = r.filter(function(val){ return val.product.brand._id.toString()==req.params.brand; })
				console.log (ok);
				res.send(ok);
			},
			function(error){
				console.log (error);
				console.log ('error');
			}
		)
	})

	router.get("/stock/all",passport.authenticate('jwt', { session: false}), function(req, res, next) {

		model.find().populate({
			path: 'product',
			populate: {
				path: 'brand',
				model: 'ProductBrandSchema'
			}
		}).exec().then (
			function(r){
				if (req.query.name && req.query.name!='') {
					var filter1 = r.filter(function (val) {
						return val.product.brand.name == req.query.name;
					})
					var filter2 = r.filter(function (val) {
						return val.product.name == req.query.name;
					})
					var filter3 = r.filter(function (val) {
						return val.product.code == req.query.name;
					})
					var ret = []
					ret.push.apply(r, filter1)
					ret.push.apply(r, filter2)
					ret.push.apply(r, filter3)
				}
				else
					ret = r;

				res.json({
					"_Array": ret
				});
			},
			function(error){
				console.log (error);
				console.log ('error');
			}
		)
	})

	router.post("/stock/update",passport.authenticate('jwt', { session: false}), function(req, res, next) {

		console.log("******************")
		var toUpdate = req.body;

		console.log(req.body)

		model.find({'_id': {$in: toUpdate.stocks.map(function(v){return mongoose.Types.ObjectId(v)}) }}).exec().then(
			function(r){
				console.log(r)
				r.forEach(function(elem){
					var inc = (1+toUpdate.increase/100);
					elem.price =  (elem.price*inc).toFixed(2);
					elem.sellPrice =  (elem.sellPrice*inc).toFixed(2)
					elem.points =  (elem.sellPrice - elem.price).toFixed(2);
					elem.save();
				})
				res.send({});
			},
			function(error){
				console.log (error);
				res.error(error)
			}
		)



	})

  router.get("/stock/product",passport.authenticate('jwt', { session: false}), function(req, res, next) {

    console.log ('stock/product')
  //hago el query por el producto
      var q=[]
      var qAnd=[]
      Object.keys(req.query).forEach(
        function(key){
          var r={}
          if (key.substr(key.length-3,3)==="_id"){
            key = key.substr(0,key.length-3)
            r[key]= mongoose.Types.ObjectId(req.query[key+"_id"].toString())
            qAnd.push( r )
          }else{
            if (req.query[key]!='')
            {
              r[key]=new RegExp(req.query[key], 'i')
              q.push( r )
            }
          }
        }
      )
      
      var query = {}
      if (qAnd.length>0){
        if (q.length>0)
          qAnd.push( {$or:q} )
        query={'$and':qAnd}
      }
      else
        if (q.length>0)
          query={$or:q}
     
      
      modelProduct.find(query).lean().distinct('_id').exec().then (
      function(products){
          try{ 
          if (products.length==0)
            res.send([]) 
          v = products.map(function(f){ return mongoose.Types.ObjectId(f.toString())})
          console.log (v);
          model.find({product: {$in : v  } }).populate('product').exec().then (
            function(ok){
              console.log (ok);
              res.send(ok);   
            },
            function(error){
              console.log (error);
              console.log ('error');
              res.error(error)
            }
          )
          }
          catch(e){
             console.log (e);
          }
        },
        function(error){
          console.log (error);
          console.log ('error');
          res.error(error)
        }
        )
    
  

})
}

var extendGetAll = function(router){
  router.get("/stock/",passport.authenticate('jwt', { session: false}), function(req, res, next) {

	  var q = []
	  var qAnd = []

	  Object.keys(req.query).forEach(
		  function (key) {
			  var r = {}
			  if (key.substr(key.length - 3, 3) === "_id") {
				  key = key.substr(0, key.length - 3)
				  r[key] = mongoose.Types.ObjectId(req.query[key + "_id"].toString())
				  qAnd.push(r)
			  } else {
				  if (req.query[key] != '' && model.schema.paths[key]) {
					  if (model.schema.paths[key].instance=='Number') {
						  if (!isNaN(req.query[key]))
						  {
							  r[key] = req.query[key]
							  q.push(r)
						  }
					  }else
					  {
						  r[key] = new RegExp(req.query[key], 'i')
						  q.push(r)
					  }

				  }
			  }
		  }
	  )

      var query = {}
      if (qAnd.length>0){
        if (q.length>0)
          qAnd.push( {$or:q} )
        query={'$and':qAnd}
      }
      else
        if (q.length>0)
          query={$or:q}

	  var _pageNumber = (req.query.page)?Number(req.query.page):0,
		  _pageSize = (req.query.pageSize)?Number(req.query.pageSize):5;

	  	  model.find()
			  .populate(
				  'product',
				  'name'
			  ).exec(function(err, docs) {

			  if (err)
		      	  res.json(err);
			  else
		      {
			      var r = docs.filter(function(f){
			      	if (req.query.name){
				        var regE = RegExp(req.query.name, 'i')
				        return  regE.test(f.product.name)
			        }

			        else
			            return true});

			      console.log(docs,r)
			      var total = r.length
			      var totalPage = (r.length>req.query.pageSize)? req.query.pageSize:r.length;
			      var offset = (req.query.page-1)*req.query.pageSize
			      var docsLeft = (r.length>req.query.pageSize)? r.slice( offset,offset+req.query.pageSize):r
			      res.json({
				      "Result": "Ok",
				      "TotalCount": total,
				      "TotalPage": totalPage,
				      "_Array": docsLeft
			      });
		      }
		  });



})
}

var routeBase = require('../baseAPISimpleRoute.js')('/stock',model,extend, { getAll:extendGetAll })


module.exports = routeBase;
