var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var model = require('../../../model/order.js')
var modelClient = require('../../../model/client.js')

var e = function (router) {

	router.get('/order/:id/info', passport.authenticate('jwt', {session: false}), function (req, res, next) {


		model.findOne({_id: req.params.id}).populate('client')
			//.populate('products.product')
			.populate({
				path: "products.product",
				populate: {
					path: "brand",
					model: "ProductBrandSchema"
				}})
			.exec().then(
			function (ok) {
				console.log(ok);
				res.send(ok);
			},
			function (error) {
				console.log(error);
				console.log('error');
			}
		)
	})

	router.get('/order/info', passport.authenticate('jwt', {session: false}), function (req, res, next) {


		var q = []
		var qAnd = []
		var matchC = []
		Object.keys(req.query).forEach(
			function (key) {
				var r = {}
				if (key.substr(key.length - 3, 3) === "_id") {
					key = key.substr(0, key.length - 3)
					r[key] = mongoose.Types.ObjectId(req.query[key + "_id"].toString())
					qAnd.push(r)
				}
				else if (key == 'dateTo') {
					r['date'] = { '$lte' : req.query[key]};
					qAnd.push(r)
				}
				else if (key === 'dateFrom') {
					r['date'] = { '$gte' : req.query[key]};
					qAnd.push(r)
				}

				else if (key.lastIndexOf('client.') === 0) {
					r[key.replace('client.','')] = new RegExp(req.query[key], 'i');
					console.log(r, key);
					matchC.push(r)
				}
				else  {
					if (req.query[key] != '' && model.schema.paths[key]) {
						if (model.schema.paths[key].instance=='Number') {
							if (!isNaN(req.query[key]))
							{
								r[key] = req.query[key]
								q.push(r)
							}
						}else
						{
							r[key] = new RegExp(req.query[key], 'i')
							q.push(r)
						}

					}
				}
			}
		)

		var query = {}
		if (qAnd.length > 0) {
			if (q.length > 0)
				qAnd.push({$or: q})
			query = {'$and': qAnd}
		}
		else if (q.length > 0)
			query = {$or: q}

		console.log(query)

		var clientPopulate = {path:'client'}

		if (matchC.length>0)
			clientPopulate["match"]={'$and' : matchC }

		model.find(query).populate(clientPopulate).populate({
			path: "products.product",
			populate: {
				path: "brand",
				model: "ProductBrandSchema"

			}}
		).exec(function(err, docs) {

			if (err)
				res.json(err);
			else {
				var r = docs.filter(function (f) {
					if (req.query.name) {
						var regE = RegExp(req.query.name, 'i')
						return regE.test(f.product.name)
					}

					else
						return true
				}).filter(function (t) {
					return t.client
				});

				console.log(docs, r)
				var total = r.length
				var totalPage = (r.length > req.query.pageSize) ? req.query.pageSize : r.length;
				var offset = (req.query.page - 1) * req.query.pageSize
				var docsLeft = (r.length > req.query.pageSize) ? r.slice(offset, offset + req.query.pageSize) : r
				res.json({
					"Result": "Ok",
					"TotalCount": total,
					"TotalPage": totalPage,
					"_Array": docsLeft
				});
			}
			/*.exec().then(
			 function (ok) {
			 res.send(ok.filter(function(t){return t.client}));
			 },
			 function (error) {
			 console.log(error);
			 console.log('error');
			 }
			 )*/
		})
	})

}

var routeBase = require('../baseAPISimpleRoute.js')('/order', model, e)


routeBase.post('/order', passport.authenticate('jwt', {session: false}), function (req, res, next) {
	model.findOne({_id: req.params.client._id}).exec().then(
		function (ok) {
			ok.calculatedPoints -= req.params.totalPoints;
			if (Number(ok.calculatedPoints.toFixed(2)) <=0)
				ok.calculatedPoints=0;
			ok.save();
			next()
		},
		function (error) {
			console.log(error);
		}
	)
})

module.exports = routeBase;
