var express = require("express");
var router = express.Router();
var passport = require("passport");

var resultDecorator = require("../../../resultDecorator.js");
var stockModel = require("../../../model/stock.js");
var orderModel = require("../../../model/order.js");

/**
 informacion de los productos
 Stock
 */
router.get("/info/products/stock/:cant?",passport.authenticate("jwt", { session: false}), function(req, res, next) {
	var filter = {}
	if (req.params.cant)
		filter = {
			'cantProducts' :{ '$lte':req.params.cant}
		}
	stockModel.find(filter)
		.populate({
				path: 'product',
				// Get friends of friends - populate the 'friends' array for every friend
				populate: { path: 'brand' }
			}
		)
		.exec().then (
		function(ok){
			console.log (ok);
			res.send(ok);
		},
		function(error){
			console.log (error);
			console.log ('error');
		}
	)

});


/**
 *prouctss
 */
router.get("/info/products/sell/:from/:to",passport.authenticate("jwt", { session: false}), function(req, res, next) {
	var filter = {}

	var dateSplit = function (dateStr) {
		var ret = [];
		ret.push( dateStr.substr(0,4) );
		ret.push( dateStr.substr(4,2)-1 );
		ret.push( dateStr.substr(6,2) );
		return ret;
	};
	var fromSplit = dateSplit (req.params.from);
	var toSplit = dateSplit(req.params.to);
	var dateTo=new Date(toSplit[0],toSplit[1],toSplit[2] );
	var dateFrom=new Date(fromSplit[0],fromSplit[1],fromSplit[2] );

	filter ={
		'$and' :
			[
				{'date': {'$lte': dateTo}},
				{'date': {'$gte': dateFrom}}
			]
	}

	orderModel.find(filter).populate(
		{
			path:'products.product',
			populate:{
				path: "brand",
				model: "ProductBrandSchema"
			}
		}
	)
		.exec().then (
		function(orders){
			try{
			products = []
			orders.forEach(function(order){
				products.push.apply(products, order.products )
			})

			productsAgg = products.filter(function(p){return p.product!=null || p.product!=undefined})
				.reduce( function(map, product){
					console.log(product)
					if (!map[product.product.name])
						map[product.product.name]={ price: 0, cant:0};

					map[product.product.name]["price"]+=product.sellPrice*product.cant;
					map[product.product.name]["cant"]+=product.cant;
					map[product.product.name]["brandName"]=product.product.brand.name;
					return map;
			}, {} )

			res.send(productsAgg);
			} catch (exception){
				console.log(exception)
				res.error(exception)

			}
		},
		function(error){
			console.log (error);
			console.log ('error');
		}
	)

});

/**
 * Busco por los productos de una marca
 */
router.get("/info/brand/:id/sell/:from/:to",passport.authenticate("jwt", { session: false}), function(req, res, next) {
	var filter = {}

	var dateSplit = function (dateStr) {
		var ret = [];
		ret.push( dateStr.substr(0,4) );
		ret.push( dateStr.substr(4,2)-1 );
		ret.push( dateStr.substr(6,2) );
		return ret;
	};
	var fromSplit = dateSplit (req.params.from);
	var toSplit = dateSplit(req.params.to);
	var dateTo=new Date(toSplit[0],toSplit[1],toSplit[2] );
	var dateFrom=new Date(fromSplit[0],fromSplit[1],fromSplit[2] );

	filter ={
		'$and' :
			[
				{'date': {'$lte': dateTo}},
				{'date': {'$gte': dateFrom}}
			]
	}

	orderModel.find(filter).populate(
		{
			path:'products.product',
			populate:{
				path: "brand",
				model: "ProductBrandSchema",
				match: {_id:req.params.id}
			}
		}
	)
		.exec().then (
		function(orders){
			products = []
			orders.forEach(function(order){
				products.push.apply(products, order.products )
			})

			productsAgg = products.filter(function(e){ return e.product!=null && e.product.brand!=null }).reduce( function(map, product){

				if (!map[product.product.name])
					map[product.product.name]={ price: 0, cant:0};

				map[product.product.name]["price"]+=product.sellPrice*product.cant;
				map[product.product.name]["cant"]+=product.cant;
				map[product.product.name]["brandName"]=product.product.brand.name;
				return map;
			}, {} )

			res.send(productsAgg);
		},
		function(error){
			console.log (error);
			console.log ('error');
		}
	)

});

module.exports = router;