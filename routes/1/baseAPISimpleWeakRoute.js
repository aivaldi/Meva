var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var passport = require('passport');

var resultDecorator = require('../../resultDecorator.js')

module.exports = function( urlBase, Model, id_parent, extend ){ 

if (extend!=null || extend!=undefined)
  extend(router);

/* GET  listing. */
router.get(urlBase+'/:id/field',passport.authenticate('jwt', { session: false}), function(req, res, next) {
    
      var model = new Model()
      var q = {}
      q[id_parent] = req.params.id
      model.getAll(q).then(
      function(result){
        res.send(resultDecorator.successDbArray( result));
      },
      function(error){
        res.send( resultDecorator.error(error));
      }
    )


});
/** GET  by id **/
router.get(urlBase+'/:id/field/:fieldId',passport.authenticate('jwt', { session: false}), function(req, res, next) {
      var model = new Model()

      model.get(req.params.fieldId).then(
      function(result){
        try{
        res.send(resultDecorator.successRecord( result));
      }catch(r)
        {console.log(r)}
      },
      function(error){
        res.send( resultDecorator.error(error));
      }
    )
});



/** new */
router.post(urlBase+'/:id/field',passport.authenticate('jwt', { session: false}), function(req, res, next) {

      req.body[id_parent] = req.params.id
      var model = new Model()

      model.save(req.body).then(
        function(result){
          res.send(resultDecorator.successInsert( result));
        },
        function(error){
          res.send( resultDecorator.error(error));
        }
      )
    
});

/** update */
router.post(urlBase+'/:id/field/:fieldId',passport.authenticate('jwt', { session: false}), function(req, res, next) {
    
      var model = new Model()

      model.save(req.body).then(
        function(result){
          res.send(resultDecorator.successInsert( result));
        },
        function(error){
          res.send( resultDecorator.error(error));
        }
      )

});

/** delete */
router.delete(urlBase+'/:id/field/:fieldId',passport.authenticate('jwt', { session: false}), function(req, res, next) {
    
        var model = new Model()

        model.delete(req.params.fieldId).then(
        function(result){
          try{
            res.send(resultDecorator.successDelete( result));
          }catch(e){
            console.log(e);
          }
        },
        function(error){
          res.send( resultDecorator.error(error));
        }
      )
});


return router;
}
