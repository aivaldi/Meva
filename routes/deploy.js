var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');

var User = mongoose.model('User');


var resultDecorator = require('../resultDecorator.js')
/**
Modelos
**/


var router = express.Router();

/* GET home page. */
router.get('/createStructures', function(req, res, next) {
  var admin = new User({
    login: 'admin',
    password: 'admin',
    active: true,
    userType : 'admin',
    lastLogin: + new Date(), 
    lastToken:'',
    authenticationType:'basic'
  })
  admin.save(function (err, small) {
    if (err) return handleError(err);
  })

  var admin2 = new User({
    login: 'test',
    password: 'test',
    active: true,
    userType : 'admin',
    lastLogin: + new Date(), 
    lastToken:'',
    authenticationType:'basic'
  })
  admin2.save(function (err, small) {
    if (err) return handleError(err);
  })

  var team = new Team({
    owner: admin,
    members: [admin],
    active: true,
    name : 'Team1',
    logo:{}
  })
  team.save(function (err, small) {
    if (err) return handleError(err);
  })

  team = new Team({
    owner: admin._id,
    members: [admin._id,admin2._id],
    active: true,
    name : 'Team2',
    logo:{}
  })
  team.save(function (err, small) {
    if (err) return handleError(err);
  })

  res.sendStatus(200);
});


var Calculation = require('../calculation/execution')
//test
router.get('/test/execution', function(req, res, next) {
  var c = new Calculation()
    c.calculate(Date.now()).then(
        function(ok){

            res.send(ok)
        },
        function(error){
            res.send(resultDecorator.error("error en calcular", error.toString()))
        }
    )

})

module.exports = router;
