var express = require('express');
var mongoose = require('mongoose');

var passport = require('passport');
var jwt = require('jwt-simple');

var LoginService = require('../services/loginService.js');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {  message:''});
});

router.get('/home',passport.authenticate('jwt', { session: false}), function(req, res, next){
  res.render('home', {  token:result });
});

router.post('/', function(req, res, next) { 
  	console.log("check for user logged", req.body);

  	var loginService = new LoginService(req.body.userName, req.body.password);
  	loginService.authenticate().then( function (result) {
      var token = jwt.encode(result, "test1234"); 
      res.render("home",{token:token})
  	}, function(error){
  		console.log(error);
  		res.render('index', {  message: 'usuario invalido o contraseña incorrecta'});
  	} )
  		
});

module.exports = router;
