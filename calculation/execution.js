const winston = require('winston');
var CalculationStatus = require('../model/calculationStatus.js');


/**
 base para el calculo de los puntos de las ordenes por cliente
 */
function OrderPointsCalculation() {
}

//Calculo obteniendo el ultimo status
OrderPointsCalculation.prototype.calculate = function (dateTo, dateFrom) {
    var me = this;
    return new Promise(function (success, error) {
        winston.log('info', 'executing getting order points on date ' + dateFrom+' ' + dateTo)

       // return CalculationStatus.findOne().then(
       //     function (res) {
	            winston.log('info', 'calculation points order for ' + dateFrom +' ' + dateTo)
                var cs = CalculationStatus()
                cs.to= dateTo
                cs.from = dateFrom
	            me.calculateUntil(cs).then(success, error)
       //     }
       //     , error
       // )

    })
}

/**
 * calculo el status a partir de un calculationStatus
 */
OrderPointsCalculation.prototype.calculateUntil = function (calculationStatus) {
    var me = this;
    var cs = calculationStatus;
    return new Promise(function (success, error) {
        winston.log('info', 'executing calculation')
        //obtengo las ordenes de compra por fecha
        cs.getOrders().then(
            function (orders) {
                winston.log('info', 'Orders fetched ok')
                winston.log('info', 'Orders # ' + orders.length)


                //con las ordenes de compra le calculo a cada cliente los puntos
                var clientPoints = me.calculatePointsByOrder(orders)

                winston.log('info', 'ClientPoints calculated ')
                winston.log('info', 'ClientPoints # ' + Object.keys(clientPoints))

                success(clientPoints)

            },
            error
        )


        //le aplico en nuevo estado o mantengo.


    })
}
/**
 * Dadas todas las ordenes de un periodo
 * Saca las ordenes por usuario
 * con cada las ordenes obtiene los puntos
 * Se los asigna al usuario que corresponde
 * Devuelvo los puntos de los usuarios en una estructura
 * {
 *  "client###":points
 * }
 * @param orders
 */
OrderPointsCalculation.prototype.calculatePointsByOrder = function (orders) {
    return orders.reduce(function (acc, key) {
        if (!acc[key.client]) acc[key.client] = 0;
        acc[key.client] += key.products.reduce(function (acc, val) {
            acc += val.points * val.cant;
            return acc
        }, 0);
        return acc
    }, {})
}




module.exports = OrderPointsCalculation