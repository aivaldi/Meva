
/**
Mapea el restulado de oracle a uno leible por la parte grafica
*/
function mapArrayDbResult(res){
	try{
	return res.rows.map(  function(row){

		var rowRet={}
		row.forEach( function (col, idx){
			rowRet[ res.metaData[idx][Object.keys(res.metaData[idx])[0] ]] = col
		})
		return rowRet
	} )
	}
	catch(e){
		console.log(e)
	}
}

function mapRecord(res){

	var r = mapArrayDbResult(res)

	if (r.length>0){
		return r[0]
	}

}


/**
Decorador para los resultados finale sobre los servicios rest
*/
var ResultDecorator = {

	error : function(msg, e){

        
		if (e==undefined)
			e = ''
		return{
			"Result":'Failure',
			'message':msg,
			'detailedMessage':e
		}
	},

	success : function(res){
		return{
			"Result":'Success',
			'value':res
		}
	},

	successDbArray : function(res){
		return{
			"Result":'Success',
			'value':mapArrayDbResult(res)
		}
	},

	successRecord : function(res){
		return{
			"Result":'Success',
			'value':mapRecord(res)
		}
	}

	,

	successInsert : function(res){
		return{
			"Result":'Success',
			'value':res.rowsAffected
		}
	}

	,

	successDelete : function(res){
		return{
			"Result":'Success',
			'value':res.rowsAffected
		}
	}

}



module.exports = ResultDecorator