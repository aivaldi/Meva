var ExceptionClass = function (_message, _code, _inner){
    var me = this
    this.message =_message
    this.code = _code
    this.inner = _inner


    this.toJson = function(){
        return {
            'message':me.message,
            'code':me.code,
            'inner':me.inner
        }
    }

}

module.exports = ExceptionClass