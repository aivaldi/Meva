var JwtStrategy = require('passport-jwt').Strategy,
 ExtractJwt = require('passport-jwt').ExtractJwt;



var mongoose = require('mongoose');
// load up the user model
var User = mongoose.model('User');
var moment = require('moment');
 moment().format();
module.exports = function(passport) {
  var opts = {};
  opts.secretOrKey = "test1234";
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
  passport.use(
    new JwtStrategy(opts, function(jwt_payload, done) {  
    
    User.findById(jwt_payload._id, 
      function (err, post) {    
        if (err) return done(err, false);        
        var date = new Date();        
      
        if((post!=undefined) && (post.lastToken == jwt_payload.lastToken) && moment(new Date(post.lastLogin)).add(30, 'm') >  moment(date) )
          { 
            post.renewToken();
            done(null, post);        
          }
        else      
          done(null, false);
  });  
  
  }));
};
