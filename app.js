var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var jwt = require('jwt-simple');
const winston = require('winston')

var resultDecorator = require('./resultDecorator.js');

winston.level = 'debug'

/**
General models
*/

var mongoose = require('mongoose');

require('./model/user.js');

var cors = require('cors')
// Use native Node promises
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/meva') //Heroku
//mongoose.connect(config.database)
  .then(() =>  console.log('connection succesful'))
  .catch((err) => console.error(err));

module.exports = {
  'secret': 'runningSecretTinkaGoes'
};


require('./jwtStrategy')(passport);


var apiVersion='1';

var deploy = require('./routes/deploy');

var index = require('./routes/index');
var users = require('./routes/users');



var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/scripts', express.static(__dirname + '/node_modules/'));
app.use('/', index);
app.use('/users', users);

app.use('/deploy', deploy);


require('./routes')(app, apiVersion);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


app.use(function(err,req, res, next) {
  console.log(err)
  winston.log('error', err);
  res.status(500)
  res.send(resultDecorator.error({message:err.toString()}, err) )
  //next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.set('port', process.env.PORT || 4000);
var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});



module.exports = app;
